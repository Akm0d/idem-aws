Idem Binary
===========

A single file binary of Idem for Linux is available at
https://repo.idemproject.io/idem/latest/linux/binary/

* Download the tar file
* Untar the downloaded file:

  .. code-block:: bash

      tar xvf idem*.tgz

* Copy the *idem*  binary to an appropriate location on your computer, such as `$HOME/bin`.

* Run the following to make the file executable:

  .. code-block:: bash

        chmod +x /path/to/idem

Now you're ready to start using Idem!

* Check your version of idem.

  .. code-block:: bash

        idem --version


Now install `idem-aws` like this:


.. code-block:: bash

   idem pip install idem-aws

.. include:: ./_includes/install_idem-aws.rst
