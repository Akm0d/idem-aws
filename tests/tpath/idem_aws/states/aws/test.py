import copy


async def acct(hub, ctx, *, name: str):
    profile = copy.deepcopy(ctx.acct)

    # Use the profile
    ret = await hub.exec.aws.ec2.vpc.get(ctx, name=name)

    # Remove "config" as it is not easily used in comparisons
    profile.pop("config")
    return dict(
        comment=ret.comment,
        old_state=None,
        new_state=profile,
        name=name,
        result=ret.result,
    )
