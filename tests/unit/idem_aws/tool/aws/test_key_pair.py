def test_convert_raw_attributes_to_present(hub, ctx):
    present_attributes = {
        "name": "test_key_name",
        "resource_id": "key-123456789",
        "public_key": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0",
        "tags": {"Enc": "test"},
    }

    raw_attributes = {
        "KeyPairId": "key-123456789",
        "KeyFingerprint": "32:32:32:32:32:32:32:32:32:32:32:32",
        "KeyName": "test_key_name",
        "KeyType": "rsa",
        "Tags": [{"Key": "Enc", "Value": "test"}],
        "PublicKey": "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0",
    }

    result = hub.tool.aws.ec2.conversion_utils.convert_raw_key_pair_to_present(
        raw_attributes
    )
    assert result == present_attributes
