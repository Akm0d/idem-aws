import asyncio
import base64
import copy
import io
import json
import os
import pathlib
import random
import string
import tempfile
import time
import uuid
import zipfile
from typing import Any
from typing import ByteString
from typing import Dict
from typing import List
from typing import Tuple

import docker
import pytest
import pytest_asyncio
from botocore.exceptions import ClientError
from Cryptodome.PublicKey import RSA
from OpenSSL import crypto


# ================================================================================
# AWS resource fixtures
# ================================================================================


@pytest.fixture(
    scope="function",
    name="__test",
    params=[0, 1, 2, 3],
    ids=["--test", "run", "no change --test", "no change"],
)
def test_flag(ctx, request):
    """
    Functions that use the __test fixture will be run four times
    0. With the --test flag set, to test what happens before creating a resource
    1. Without the --test flag set, to create a resource
    2. With the --test flag set, to verify that no changes would be made
    3. Without the --test flag set, to verify that no changes are made
    """
    ctx.test = bool((request.param + 1) % 2)
    yield request.param


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_subnet(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    subnet_temp_name = "idem-fixture-subnet-" + str(int(time.time()))
    az = ctx["acct"].get("region_name") + "b"
    ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 28
    )
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        name=subnet_temp_name,
        cidr_block=ipv4_cidr_sub_block,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        availability_zone=az,
        tags=[{"Key": "Name", "Value": subnet_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Subnet", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)

    yield after

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=after["SubnetId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="module")
async def aws_ec2_key_pair(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 Key Pair
    :return: a description of an ec2 key pair
    """
    key_pair_name_temp = "idem-fixture-key-pair-" + str(int(time.time()))
    public_key = (
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP"
        "/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4"
        "+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO"
        "/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9"
        "+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 "
        "email@example.com"
    )
    present_ret = await hub.states.aws.ec2.key_pair.present(
        ctx,
        name=key_pair_name_temp,
        public_key=public_key,
    )
    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")

    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.ec2.key_pair.absent(
        ctx,
        resource_id=new_state.get("resource_id"),
        name=new_state.get("name"),
    )
    assert absent_ret["result"], absent_ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_subnet_2(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 subnet for a module that needs it
    :return: a description of an ec2 subnet
    """
    # Embed unix timestamp in the same so we know when the resource was created when looking at an AWS endpoint.
    subnet_temp_name = f"idem-fixture-subnet-{int(time.time())}"
    az = ctx["acct"].get("region_name") + "a"
    for i in range(5):
        # Generate a CidrBlock to provision a subnet. If the CidrBlock has been taken(InvalidSubnet.Conflict),
        # try another CidrBlock.
        ipv4_cidr_sub_block = hub.tool.utils.get_sub_cidr_block(
            aws_ec2_vpc.get("CidrBlock"), 28
        )
        ret = await hub.states.aws.ec2.subnet.present(
            ctx,
            name=subnet_temp_name,
            cidr_block=ipv4_cidr_sub_block,
            vpc_id=aws_ec2_vpc.get("VpcId"),
            availability_zone=az,
            tags=[{"Key": "Name", "Value": subnet_temp_name}],
        )
        if (not ret["result"]) and "InvalidSubnet.Conflict" in str(ret["comment"]):
            continue
        break
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Subnet", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)

    yield after

    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=after["SubnetId"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_vpc(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_temp_name = "idem-fixture-vpc-1-" + str(int(time.time()))
    cidr_block = os.getenv("IT_FIXTURE_EC2_VPC_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"192.168.{num()}.0/24"
    cidr_block_association_set = [{"CidrBlock": cidr_block}]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=[{"Key": "Name", "Value": vpc_temp_name}],
        enable_dns_hostnames=True,
        enable_dns_support=True,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Vpc", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=after["VpcId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_vpc_2(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 vpc for a module that needs it
    :return: a description of an ec2 vpc
    """
    vpc_temp_name = "idem-fixture-vpc-2-" + str(int(time.time()))
    cidr_block = os.getenv("IT_FIXTURE_EC2_VPC_CIDR_BLOCK")
    num: () = lambda: random.randint(0, 255)
    if cidr_block is None:
        cidr_block = f"192.168.{num()}.0/24"
    cidr_block_association_set = [{"CidrBlock": cidr_block}]
    ret = await hub.states.aws.ec2.vpc.present(
        ctx,
        name=vpc_temp_name,
        cidr_block_association_set=copy.deepcopy(cidr_block_association_set),
        tags=[{"Key": "Name", "Value": vpc_temp_name}],
        enable_dns_hostnames=True,
        enable_dns_support=True,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Vpc", ret.get("new_state").get("resource_id")
    )
    after = await hub.tool.boto3.resource.describe(resource)
    assert after

    yield after

    ret = await hub.states.aws.ec2.vpc.absent(
        ctx, name=vpc_temp_name, resource_id=after["VpcId"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_elastic_ip(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 Elastic IP address for a module that needs it
    :return: a description of an EC2 Elastic IP address
    """
    elastip_ip_temp_name = "idem-fixture-elastic-ip-" + str(int(time.time()))
    ret = await hub.states.aws.ec2.elastic_ip.present(
        ctx,
        name=elastip_ip_temp_name,
        domain="vpc",
        tags=[{"Key": "Name", "Value": elastip_ip_temp_name}],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    yield after

    ret = await hub.states.aws.ec2.elastic_ip.absent(
        ctx, name=elastip_ip_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_ami_pro(hub, ctx) -> Dict[str, Any]:
    if hub.tool.utils.is_running_localstack(ctx):
        # Search for an ami that exists in docker: https://docs.localstack.cloud/aws/elastic-compute-cloud/
        # For localstack tests, create a tag like this on an ubuntu:focal container:
        #     $ docker tag ubuntu:focal localstack-ec2/idem-test-ami:ami-000001
        image_id = await hub.states.aws.ec2.ami.search(
            ctx,
            name=None,
            most_recent=False,
            filters=[{"name": "name", "values": ["idem-test-ami"]}],
        )
        # Image from docker exists, use that for localstack
        if image_id["result"]:
            return image_id["new_state"]
    else:
        # Find an ami that works with the ec2 instance fixture's instance type
        image_id = await hub.states.aws.ec2.ami.search(
            ctx,
            name=None,
            most_recent=True,
            owners=["amazon"],
            filters=[
                {"name": "image-type", "values": ["machine"]},
                {"name": "state", "values": ["available"]},
                {"name": "hypervisor", "values": ["xen"]},
                {"name": "architecture", "values": ["x86_64"]},
                {"name": "root-device-type", "values": ["ebs"]},
                {"name": "virtualization-type", "values": ["hvm"]},
            ],
        )

        # Image from docker exists, use that for localstack
        if image_id["result"]:
            return image_id["new_state"]

    raise pytest.skip("No ec2 ami could be found that meets the requirements")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_instance_type(hub, ctx) -> Dict[str, Any]:
    instance_type = await hub.exec.aws.ec2.instance_type.get(
        ctx,
        name=None,
        filters=[
            {"name": "instance-type", "values": ["*.nano"]},
            {"name": "hypervisor", "values": ["xen"]},
            {"name": "processor-info.supported-architecture", "values": ["x86_64"]},
        ],
    )
    if not instance_type["result"]:
        raise pytest.skip(
            "No ec2 instance type could be found that meets the requirements"
        )

    return instance_type["ret"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_instance(
    hub,
    ctx,
    instance_name,
    aws_ec2_instance_type,
    aws_ec2_ami_pro,
) -> Dict[str, Any]:
    """
    Create a light ec2 instance that can be used in tests
    """
    ret = await hub.states.aws.ec2.instance.present(
        ctx,
        name=instance_name,
        # Don't specify a subnet_id to use the default subnet
        # subnet_id=aws_ec2_subnet.get("SubnetId"),
        instance_type=aws_ec2_instance_type["resource_id"],
        image_id=aws_ec2_ami_pro["resource_id"],
        client_token=instance_name,
        tags={"Name": instance_name},
    )

    assert ret["result"], ret["comment"]
    assert ret.get("new_state"), ret["comment"]

    resource_id = ret["new_state"]["resource_id"]
    resource = await hub.tool.boto3.resource.create(ctx, "ec2", "Instance", resource_id)
    await hub.tool.boto3.resource.exec(resource, "wait_until_exists")
    await hub.tool.boto3.resource.exec(resource, "wait_until_running")

    get_ret = None
    while not get_ret:
        get_ret = await hub.exec.aws.ec2.instance.get(
            ctx, resource_id=resource.id, name=instance_name
        )

    yield ret["new_state"]

    ret = await hub.states.aws.ec2.instance.absent(
        ctx, name=instance_name, resource_id=resource_id, client_token=instance_name
    )
    await hub.tool.boto3.resource.exec(resource, "wait_until_terminated")
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_iam_instance_profile(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 placement group
    :return: a description of an ec2 placement group
    """
    instance_profile_name = "idem-fixture-instance-profile-" + str(uuid.uuid4())
    instance_profile_tags = {"Name": instance_profile_name}
    ret = await hub.states.aws.iam.instance_profile.present(
        ctx,
        name=instance_profile_name,
        tags=instance_profile_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    yield after

    ret = await hub.states.aws.iam.instance_profile.absent(
        ctx, name=instance_profile_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_placement_group(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 placement group
    :return: a description of an ec2 placement group
    """
    placement_group_name = "idem-fixture-placement-group-" + str(uuid.uuid4())
    placement_group_tags = {"Name": placement_group_name}
    ret = await hub.states.aws.ec2.placement_group.present(
        ctx,
        name=placement_group_name,
        strategy="spread",
        tags=placement_group_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    yield after

    ret = await hub.states.aws.ec2.placement_group.absent(
        ctx, name=placement_group_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_transit_gateway(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 transit gateway for a module that needs it
    :return: a description of an ec2 transit gateway
    """
    transit_gateway_temp_name = "idem-fixture-transit-gateway-" + str(uuid.uuid4())
    transit_gateway_tags = [{"Key": "Name", "Value": transit_gateway_temp_name}]
    description = "idem-fixture-transit-gateway"
    ret = await hub.states.aws.ec2.transit_gateway.present(
        ctx,
        name=transit_gateway_temp_name,
        description=description,
        tags=transit_gateway_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_state(
        ctx, resource_id, "available"
    )

    yield after

    ret = await hub.states.aws.ec2.transit_gateway.absent(
        ctx, name=transit_gateway_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    await hub.tool.ec2_test_util.wait_for_transit_gateway_state(
        ctx, resource_id, "deleted"
    )


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_nat_gateway(hub, ctx, aws_ec2_subnet) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 NAT gateway for a module that needs it
    :return: a description of an EC2 NAT Gateway
    """
    nat_gateway_name = "idem-fixture-nat-gateway-" + str(uuid.uuid4())
    tags = [
        {"Key": "Name", "Value": nat_gateway_name},
    ]
    ret = await hub.states.aws.ec2.nat_gateway.present(
        ctx,
        name=nat_gateway_name,
        subnet_id=aws_ec2_subnet.get("SubnetId"),
        client_token=None,
        connectivity_type="private",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")
    yield after

    # Delete instance
    ret = await hub.states.aws.ec2.nat_gateway.absent(
        ctx,
        name=nat_gateway_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and (not ret.get("new_state"))


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_security_group(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 security group for a module that needs it
    :return: a description of an ec2 security group
    """
    security_group_temp_name = "idem-fixture-security-group-" + str(uuid.uuid4())
    security_group_tags = [{"Key": "Name", "Value": security_group_temp_name}]
    description = "Security group fixture resource for Idem integration test."
    ret = await hub.states.aws.ec2.security_group.present(
        ctx,
        name=security_group_temp_name,
        description=description,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=security_group_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.security_group.absent(
        ctx, name=security_group_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_spot_instance_request(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 security group for a module that needs it
    :return: a description of an ec2 security group
    """
    spot_instance_request_temp_name = "idem-fixture-security-group-" + str(uuid.uuid4())
    spot_instance_request_tags = [
        {"Key": "Name", "Value": spot_instance_request_temp_name}
    ]
    description = "Security group fixture resource for Idem integration test."
    ret = await hub.states.aws.ec2.spot_instance_request.present(
        ctx,
        name=spot_instance_request_temp_name,
        description=description,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=spot_instance_request_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.spot_instance_request.absent(
        ctx, name=spot_instance_request_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(False)
async def aws_ec2_default_vpc(hub, ctx) -> Dict[str, Any]:
    vpc_ret = await hub.exec.boto3.client.ec2.describe_vpcs(ctx)
    after = {}
    if vpc_ret["result"]:
        for vpc in vpc_ret["ret"]["Vpcs"]:
            if vpc.get("IsDefault"):
                after["default_vpc_id"] = vpc.get("VpcId")
                break
    yield after


@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(False)
async def aws_ec2_default_subnets(hub, ctx) -> Dict[str, Any]:
    # ('ClientError: An error occurred (ValidationError) when calling the CreateLoadBalancer operation: At least
    # two subnets in two different Availability Zones must be specified',)
    after = {}
    subnets = await hub.exec.boto3.client.ec2.describe_subnets(ctx)
    if subnets["result"]:
        for subnet in subnets["ret"]["Subnets"]:
            if subnet.get("DefaultForAz"):
                if (
                    subnet.get("AvailabilityZone")
                    == ctx["acct"].get("region_name") + "a"
                ):
                    after["id_a_subnet"] = subnet.get("SubnetId")
                if (
                    subnet.get("AvailabilityZone")
                    == ctx["acct"].get("region_name") + "b"
                ):
                    after["id_b_subnet"] = subnet.get("SubnetId")
                if (
                    subnet.get("AvailabilityZone")
                    == ctx["acct"].get("region_name") + "c"
                ):
                    after["id_c_subnet"] = subnet.get("SubnetId")
    yield after


@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(False)
async def aws_ec2_default_internet_gateway(
    hub, ctx, aws_ec2_default_vpc
) -> Dict[str, Any]:
    # At least one IGW should be attached to the default VPC. Else, create LB call fails woth this error.
    # 'InvalidSubnetException: An error occurred (InvalidSubnet) when calling the CreateLoadBalancer
    # operation: VPC vpc-092d586bd79487f84 has no internet gateway'
    after = {}
    i_gateways = await hub.exec.boto3.client.ec2.describe_internet_gateways(ctx)
    if i_gateways["result"]:
        for i_gateway in i_gateways["ret"]["InternetGateways"]:
            if i_gateway.get("Attachments"):
                if i_gateway.get("Attachments")[0].get(
                    "VpcId"
                ) == aws_ec2_default_vpc.get("default_vpc_id"):
                    after["internet_gateway_id"] = i_gateway.get("InternetGatewayId")
                    break
    yield after


@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(False)
async def aws_elbv2_load_balancer(hub, ctx, aws_ec2_default_subnets) -> Dict[str, Any]:
    assert aws_ec2_default_subnets
    assert aws_ec2_default_subnets.get("id_a_subnet"), aws_ec2_default_subnets.get(
        "id_b_subnet"
    )

    elbv2_lb_temp_name = "idem-fixture-elbv2-lb-" + str(int(time.time()))
    elbv2_lb_tags = {"Name": elbv2_lb_temp_name}
    response = await hub.states.aws.elbv2.load_balancer.present(
        ctx,
        name=elbv2_lb_temp_name,
        tags=elbv2_lb_tags,
        subnets=[
            aws_ec2_default_subnets.get("id_a_subnet"),
            aws_ec2_default_subnets.get("id_b_subnet"),
        ],
        scheme="internet-facing",
        lb_type="application",
        ip_address_type="ipv4",
    )
    assert response["result"], response["comment"]
    assert hub.tool.aws.comment_utils.create_comment(
        resource_type="aws.elbv2.load_balancer", name=elbv2_lb_temp_name
    )
    assert not response.get("old_state") and response.get("new_state")
    after = response.get("new_state")

    yield after

    response = await hub.states.aws.elbv2.load_balancer.absent(
        ctx,
        name=elbv2_lb_temp_name,
        resource_id=after["resource_id"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and not response.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_internet_gateway(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 internet gateway for a module that needs it
    :return: a description of an ec2 internet gateway
    """
    internet_gateway_temp_name = "idem-fixture-internet-gateway-" + str(uuid.uuid4())
    internet_gateway_tags = [{"Key": "Name", "Value": internet_gateway_temp_name}]
    ret = await hub.states.aws.ec2.internet_gateway.present(
        ctx,
        name=internet_gateway_temp_name,
        vpc_id=[aws_ec2_vpc.get("VpcId")],
        tags=internet_gateway_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.internet_gateway.absent(
        ctx, name=internet_gateway_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_route_table(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 route table for a module that needs it
    :return: a description of an ec2 route table
    """
    route_table_name = "idem-fixture-route-table-" + str(uuid.uuid4())
    route_table_tags = [{"Key": "Name", "Value": route_table_name}]
    ret = await hub.states.aws.ec2.route_table.present(
        ctx,
        name=route_table_name,
        vpc_id=aws_ec2_vpc.get("VpcId"),
        tags=route_table_tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.route_table.absent(
        ctx, name=route_table_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_dhcp_options(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 DHCP options for a module that needs it.
    :return: a description of DHCP options.
    """
    # Create dhcp_option
    name = "idem-fixture-dhcp-option-" + str(uuid.uuid4())
    # tags can be passed as list or dict
    tags = {"Name": name}
    dhcp_configurations = [{"Key": "domain-name-servers", "Values": ["10.2.5.1"]}]

    ret = await hub.states.aws.ec2.dhcp_option.present(
        ctx,
        name=name,
        dhcp_configurations=dhcp_configurations,
        vpc_id=None,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.ec2.dhcp_option '{name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # Delete real dhcp
    ret = await hub.states.aws.ec2.dhcp_option.absent(
        ctx, name=name, resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert f"Deleted aws.ec2.dhcp_option '{name}'" in ret["comment"]


@pytest_asyncio.fixture(scope="module")
@pytest.mark.localstack(False)
async def aws_elbv2_target_group(hub, ctx, aws_ec2_default_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an ELBv2 Target Group for a module that needs it
    :return: a description of an ELBv2 Target Group.
    """
    elbv2_tg_temp_name = "idem-fixture-elbv2-tg-" + str(int(time.time()))
    elbv2_tg_tags = {"Name": elbv2_tg_temp_name}
    response = await hub.states.aws.elbv2.target_group.present(
        ctx,
        name=elbv2_tg_temp_name,
        tags=elbv2_tg_tags,
        protocol="HTTP",
        port=80,
        protocol_version="HTTP1",
        ip_address_type="ipv4",
        vpc_id=aws_ec2_default_vpc.get("default_vpc_id"),
    )
    assert response["result"], response["comment"]
    assert hub.tool.aws.comment_utils.create_comment(
        resource_type="aws.elbv2.target_group", name=elbv2_tg_temp_name
    )
    assert not response.get("old_state") and response.get("new_state")
    after = response.get("new_state")

    yield after

    response = await hub.states.aws.elbv2.target_group.absent(
        ctx,
        name=elbv2_tg_temp_name,
        resource_id=after["resource_id"],
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and not response.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_role(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role for a module that needs it
    :return: a description of an IAM role
    """
    role_name = "idem-fixture-role-" + str(int(time.time()))
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Principal": {"Service": [ "spot.amazonaws.com", "lambda.amazonaws.com" ]},"Action": "sts:AssumeRole"}}'
    description = "Idem IAM role integration test fixture"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # role should be deleted by name (not resource_id)
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_role_with_tags(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role for a module that needs it
    :return: a description of an IAM role
    """
    role_name = "idem-fixture-role-" + str(uuid.uuid4())
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Principal": {"Service": [ "spot.amazonaws.com", "lambda.amazonaws.com" ]},"Action": "sts:AssumeRole"}}'
    description = "Idem IAM role integration test fixture"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
        tags=hub.tool.aws.tag_utils.convert_tag_dict_to_list({"Name": role_name}),
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # role should be deleted by name (not resource_id)
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_role_2(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM role for a module that needs it
    :return: a description of an IAM role
    """
    role_name = "idem-fixture-role-2-" + str(int(time.time()))
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": {"Effect": "Allow","Principal": {"Service": ["spot.amazonaws.com", "lambda.amazonaws.com", "events.amazonaws.com"]},"Action": "sts:AssumeRole"}}'
    description = "Idem IAM role integration test fixture 2"
    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # role should be deleted by name (not resource_id)
    ret = await hub.states.aws.iam.role.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_user(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM user for a module that needs it
    :return: a description of an IAM user
    """
    user_name = "idem-fixture-user-" + str(int(time.time()))
    ret = await hub.states.aws.iam.user.present(
        ctx, name=user_name, user_name=user_name
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.iam.user.absent(
        ctx, name=after.get("name"), resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_iam_user_2(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM user for a module that needs it
    :return: a description of an IAM user
    """
    user_name = "idem-fixture-user-2-" + str(int(time.time()))
    ret = await hub.states.aws.iam.user.present(
        ctx, name=user_name, user_name=user_name
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.iam.user.absent(
        ctx, name=after.get("name"), resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_lambda_function(hub, ctx, zip_file, aws_iam_role_2) -> Dict[str, Any]:
    function_name = "test_idem_aws_function_" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )

    # this is to ensure the created role 'aws_iam_role_2' is fully functional and is ready to be
    # assumed by Lambda service. Since role doesn't have a status field, tests are repeatedly failing
    # with exception: 'InvalidParameterValueException: An error occurred (InvalidParameterValueException) when calling
    #                  the CreateFunction operation: The role defined for the function cannot be assumed by Lambda.',)
    if not hub.tool.utils.is_running_localstack(ctx):
        time.sleep(45)

    ret = await hub.states.aws.lambda_aws.function.present(
        ctx,
        name=function_name,
        runtime="python3.7",
        handler="code.main",
        role=aws_iam_role_2["arn"],
        code={"ZipFile": zip_file},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.lambda_aws.function.absent(
        ctx, name=function_name, resource_id=function_name
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.lambda.function '{function_name}'" in ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_lambda_function_publish_version(
    hub, ctx, aws_lambda_function
) -> Dict[str, Any]:
    # Provisioned Concurrency Configs cannot be applied to unpublished function versions.
    # Publish lambda function and apply Concurrency Config on this version.
    function_name = aws_lambda_function["name"]
    publish_ret = await hub.exec.boto3.client["lambda"].publish_version(
        ctx, FunctionName=function_name
    )
    after = aws_lambda_function
    if publish_ret["result"]:
        function = publish_ret["ret"]
        after["version"] = function.get("Version")
        yield after
    else:
        hub.log.error({publish_ret["comment"]})


@pytest_asyncio.fixture(scope="module")
async def aws_kms_key(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an KMS key for a module that needs it
    :return: a description of an KMS key
    """
    CI_ACCT_NUM = os.getenv("CI_ACCT_NUM")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert (
            CI_ACCT_NUM
        ), "AWS account number has to be supplied as CI_ACCT_NUM to run this test."
    CI_ACCT_NUM = CI_ACCT_NUM if CI_ACCT_NUM else "6767676767"
    print(f"Going to use account '{CI_ACCT_NUM}' for KMS key")

    key_name = "idem-fixture-kms-key-" + str(uuid.uuid4())
    ret = await hub.states.aws.kms.key.present(
        ctx,
        key_name,
        policy='{ "Version" : "2012-10-17",  "Id" : "key-consolepolicy-3", '
        '"Statement" : [{"Sid" : "Enable IAM User Permissions","Effect" : "Allow", '
        '"Principal" : {"AWS" : "arn:aws:iam::'
        f"{CI_ACCT_NUM}"
        ':root"},"Action" : [ "kms:Create*", "kms:Describe*", '
        '"kms:Enable*", "kms:List*", "kms:Put*", "kms:Update*", "kms:ScheduleKeyDeletion", "kms:Revoke*", '
        '"kms:Disable*", "kms:Get*", "kms:Delete*", "kms:TagResource", "kms:UntagResource" ], "Resource" : "*" }]}',
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after
    ret = await hub.states.aws.kms.key.absent(
        ctx, name=key_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state")


@pytest_asyncio.fixture(scope="module")
async def aws_s3_bucket(hub, ctx) -> Dict[str, Any]:
    bucket_name = "idem-fixture-bucket-" + str(uuid.uuid4())

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_name,
        create_bucket_configuration={
            "LocationConstraint": ctx["acct"].get("region_name")
        },
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # S3 Bucket is not getting deleted until it is empty, so deleted all the objects in bucket first.
    await deleteBucketObjects(hub, ctx, bucket_name)

    ret = await hub.states.aws.s3.bucket.absent(
        ctx,
        name=after.get("name"),
        resource_id=after.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_s3_bucket_2(hub, ctx) -> Dict[str, Any]:
    bucket_name = "idem-fixture-bucket-2-" + str(int(time.time()))

    ret = await hub.states.aws.s3.bucket.present(
        ctx,
        name=bucket_name,
        create_bucket_configuration={
            "LocationConstraint": ctx["acct"].get("region_name")
        },
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    # S3 Bucket is not getting deleted until it is empty, so deleted all the objects in bucket first.
    await deleteBucketObjects(hub, ctx, bucket_name)

    ret = await hub.states.aws.s3.bucket.absent(
        ctx,
        name=after.get("name"),
        resource_id=after.get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


async def deleteBucketObjects(hub, ctx, bucket_name: str):
    ret = await hub.exec.boto3.client.s3.list_objects(ctx, Bucket=bucket_name)
    if ret["result"]:
        for bucket_object in ret["ret"]["Contents"]:
            key = bucket_object.get("Key")
            result = await hub.exec.boto3.client.s3.delete_object(
                ctx, Bucket=bucket_name, Key=key
            )
            if not result["result"]:
                hub.log.error({ret["comment"]})
    else:
        hub.log.error({ret["comment"]})


@pytest_asyncio.fixture(scope="module")
async def aws_route53_private_hosted_zone(hub, ctx, aws_ec2_vpc) -> Dict[str, Any]:
    # localstack is not supporting creation of private hosted zones. It gives error 500 Internal server error.
    # We can only attach vpc to private hosted zones. so this test can be only run on real AWS.
    if hub.tool.utils.is_running_localstack(ctx):
        yield None
    else:
        name = "idem-fixture-hosted-zone-" + str(uuid.uuid4())

        # create hosted zone

        result = await hub.states.aws.route53.hosted_zone.present(
            ctx,
            name=name,
            caller_reference=str(uuid.uuid4()),
            hosted_zone_name=name,
            config={"PrivateZone": True},
            vpcs=[
                {
                    "VPCId": aws_ec2_vpc.get("VpcId"),
                    "VPCRegion": ctx["acct"].get("region_name"),
                }
            ],
        )
        assert result["result"], result["comment"]
        after = result.get("new_state")

        yield after

        resource_id = after.get("resource_id")
        ret = await hub.states.aws.route53.hosted_zone.absent(
            ctx, name=name, resource_id=resource_id
        )

        assert ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_organization(hub, ctx) -> Dict[str, Any]:
    name = "idem-fixture-aws-organization-" + str(uuid.uuid4())

    # create organization
    result = await hub.states.aws.organizations.organization.present(
        ctx, name=name, feature_set="ALL"
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.organization.absent(ctx, name=name)

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_organization_unit(hub, ctx, aws_organization) -> Dict[str, Any]:
    ou_name = "idem-fixture-organization-unit-" + str(uuid.uuid4())
    roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)

    root_id = None

    if roots_resp:
        root_id = roots_resp["ret"]["Roots"][0]["Id"]

    create_tag = [{"Key": "org", "Value": "idem"}, {"Key": "env", "Value": "test"}]
    # create organization unit
    result = await hub.states.aws.organizations.organization_unit.present(
        ctx, name=ou_name, parent_id=root_id, tags=create_tag
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.organization_unit.absent(
        ctx,
        name=result.get("new_state").get("name"),
        resource_id=result.get("new_state").get("resource_id"),
    )

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_organization_policy(hub, ctx, aws_organization) -> Dict[str, Any]:
    if hub.tool.utils.is_running_localstack(ctx):
        yield None
    else:
        policy_name = "idem-fixture-organizations-policy-" + str(uuid.uuid4())
        description = (
            "Enables admins of attached accounts to delegate all S3 permissions"
        )
        policy_type = "SERVICE_CONTROL_POLICY"
        content = '{"Version": "2012-10-17","Statement": [{"Effect": "Deny","Action": ["s3:DeleteBucket", "s3:DeleteObject", "s3:DeleteObjectVersion"], "Resource": "*"}]}'

        create_tag = [{"Key": "org", "Value": "idem"}]

        result = await hub.states.aws.organizations.policy.present(
            ctx,
            name=policy_name,
            description=description,
            policy_type=policy_type,
            content=content,
            tags=create_tag,
        )
        if not hub.tool.utils.is_running_localstack(ctx):
            assert result["result"], result["comment"]
        after = result.get("new_state")

        yield after

        ret = await hub.states.aws.organizations.policy.absent(
            ctx,
            name=result.get("new_state").get("name"),
            resource_id=result.get("new_state").get("resource_id"),
        )

        assert ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_sns_topic(hub, ctx) -> Dict[str, Any]:
    topic_name = "idem-fixture-topic-" + str(uuid.uuid4())
    delivery_policy = json.dumps(
        {
            "http": {
                "defaultHealthyRetryPolicy": {
                    "minDelayTarget": 10,
                    "maxDelayTarget": 30,
                    "numRetries": 10,
                    "numMaxDelayRetries": 7,
                    "numNoDelayRetries": 0,
                    "numMinDelayRetries": 3,
                    "backoffFunction": "linear",
                },
                "disableSubscriptionOverrides": False,
            }
        },
        separators=(",", ":"),
    )
    attributes = {"DeliveryPolicy": delivery_policy}
    tags = [{"Key": "Name", "Value": topic_name}]

    ret = await hub.states.aws.sns.topic.present(
        ctx, name=topic_name, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.sns.topic.absent(
        ctx, name=topic_name, resource_id=new_state.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.sns.topic '{topic_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_sns_topic_2(hub, ctx) -> Dict[str, Any]:
    topic_name = "idem-fixture-topic-" + str(uuid.uuid4())
    delivery_policy = json.dumps(
        {
            "http": {
                "defaultHealthyRetryPolicy": {
                    "minDelayTarget": 10,
                    "maxDelayTarget": 30,
                    "numRetries": 10,
                    "numMaxDelayRetries": 7,
                    "numNoDelayRetries": 0,
                    "numMinDelayRetries": 3,
                    "backoffFunction": "linear",
                },
                "disableSubscriptionOverrides": False,
            }
        },
        separators=(",", ":"),
    )
    attributes = {"DeliveryPolicy": delivery_policy}
    tags = [{"Key": "Name", "Value": topic_name}]

    ret = await hub.states.aws.sns.topic.present(
        ctx, name=topic_name, attributes=attributes, tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.sns.topic.absent(
        ctx, name=topic_name, resource_id=new_state.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.sns.topic '{topic_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_rds_db_subnet_group(hub, ctx, aws_ec2_subnet, aws_ec2_subnet_2):
    # To skip running on localstack as it is only working with real aws
    if hub.tool.utils.is_running_localstack(ctx):
        yield None
    else:
        # Create db_subnet_group
        db_subnet_group_name = "idem-fixture-db-subnet-group-" + str(uuid.uuid4())
        tags = [
            {"Key": "Name", "Value": db_subnet_group_name},
        ]
        ret = await hub.states.aws.rds.db_subnet_group.present(
            ctx,
            name=db_subnet_group_name,
            db_subnet_group_description="idem fixture",
            subnets=[
                aws_ec2_subnet.get("SubnetId"),
                aws_ec2_subnet_2.get("SubnetId"),
            ],
            tags=tags,
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("old_state") and ret.get("new_state")
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.rds.db_subnet_group", name=db_subnet_group_name
            )[0]
            in ret["comment"]
        )
        new_state = ret["new_state"]

        yield new_state

        ret = await hub.states.aws.rds.db_subnet_group.absent(
            ctx, name=db_subnet_group_name, resource_id=new_state["resource_id"]
        )
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.rds.db_subnet_group", name=db_subnet_group_name
            )[0]
            in ret["comment"]
        )
        assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_cloudwatch_log_group(hub, ctx) -> Dict[str, Any]:
    name = "idem-fixture-log-group-" + str(uuid.uuid4())
    ret = await hub.states.aws.cloudwatch.log_group.present(ctx, name=name)
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.cloudwatch.log_group", name=name
        )[0]
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    new_state = ret["new_state"]

    yield new_state

    ret = await hub.states.aws.cloudwatch.log_group.absent(
        ctx, name=name, resource_id=new_state["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.cloudwatch.log_group", name=name
        )[0]
        in ret["comment"]
    )
    assert ret["old_state"] and not ret["new_state"]


@pytest_asyncio.fixture(scope="module")
async def aws_lambda_function_permission(
    hub, ctx, aws_lambda_function
) -> Dict[str, Any]:
    name = "idem-fixture-lambda-permission-" + str(int(time.time()))
    action = "lambda:*"
    function_name = aws_lambda_function.get("name")
    principal = "logs.amazonaws.com"

    ret = await hub.states.aws.lambda_aws.function_permission.present(
        ctx,
        action=action,
        principal=principal,
        name=name,
        function_name=function_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.lambda_aws.function_permission.absent(
        ctx,
        name=name,
        function_name=function_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_lambda_elb_target_group_permissions(
    hub, ctx, aws_lambda_function
) -> Dict[str, Any]:
    name = "idem-fixture-elbv2-tg-lambda-permission-" + str(uuid.uuid4())
    action = "lambda:InvokeFunction"
    function_name = aws_lambda_function.get("name")
    principal = "elasticloadbalancing.amazonaws.com"

    ret = await hub.states.aws.lambda_aws.function_permission.present(
        ctx,
        action=action,
        principal=principal,
        name=name,
        function_name=aws_lambda_function.get("name"),
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.lambda_aws.function_permission.absent(
        ctx,
        name=name,
        function_name=function_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.mark.localstack(
    False,
    "To skip running on localstack as rds_db_cluster is only working with real aws",
)
@pytest_asyncio.fixture(scope="module")
async def aws_rds_db_cluster(hub, ctx, aws_rds_db_subnet_group) -> Dict[str, Any]:
    name = "idem-fixture-db-cluster" + str(int(time.time()))
    db_subnet_group_name = aws_rds_db_subnet_group.get("resource_id")
    ret = await hub.states.aws.rds.db_cluster.present(
        ctx,
        name=name,
        engine="aurora-mysql",
        master_username="admin123",
        master_user_password="abcd1234",
        db_subnet_group_name=db_subnet_group_name,
    )
    assert ret["result"], ret["comment"]
    assert f"Created aws.rds.db_cluster '{name}'" in ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.rds.db_cluster.absent(
        ctx,
        name=name,
        skip_final_snapshot=True,
        resource_id=new_state["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.rds.db_cluster '{name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_s3_bucket_version(hub, ctx, aws_s3_bucket) -> Dict[str, Any]:
    bucket_name = aws_s3_bucket.get("name")
    status = "Enabled"
    bucket_versioning_temp_name = "idem-test-bucket-versioning-" + str(int(time.time()))

    result = await hub.states.aws.s3.bucket_versioning.present(
        ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket_name,
        status=status,
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.s3.bucket_versioning.absent(
        ctx,
        name=after.get("Name"),
        resource_id=after.get("resource_id"),
    )

    assert ret["comment"]
    assert ret.get("old_state")


@pytest_asyncio.fixture(scope="module")
async def aws_s3_bucket_version_2(hub, ctx, aws_s3_bucket_2) -> Dict[str, Any]:
    bucket_name = aws_s3_bucket_2.get("name")
    status = "Enabled"
    bucket_versioning_temp_name = "idem-test-bucket-versioning-" + str(int(time.time()))

    result = await hub.states.aws.s3.bucket_versioning.present(
        ctx,
        name=bucket_versioning_temp_name,
        bucket=bucket_name,
        status=status,
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.s3.bucket_versioning.absent(
        ctx,
        name=after.get("Name"),
        resource_id=after.get("resource_id"),
    )

    assert ret["comment"]
    assert ret.get("old_state")


@pytest_asyncio.fixture(scope="module")
async def aws_s3_bucket_policy(hub, ctx, aws_s3_bucket) -> Dict[str, Any]:
    bucket_name = aws_s3_bucket.get("name")
    bucket_policy_temp_name = f"{bucket_name}-policy"

    policy_to_add = (
        '{"Version":"2012-10-17","Statement":[{"Sid":"AWSCloudTrailAclCheck20150319","Effect":"Allow","Principal":{"Service": "cloudtrail.amazonaws.com"},"Action":"s3:GetBucketAcl","Resource":"arn:aws:s3:::'
        + bucket_name
        + '"},{"Sid":"AWSCloudTrailWrite20150319","Effect":"Allow","Principal":{"Service": "cloudtrail.amazonaws.com"},"Action":"s3:PutObject", "Resource": "arn:aws:s3:::'
        + bucket_name
        + '/AWSLogs/*/*","Condition": {"StringEquals": {"s3:x-amz-acl": "bucket-owner-full-control"}}}]}'
    )
    result = await hub.states.aws.s3.bucket_policy.present(
        ctx,
        name=bucket_policy_temp_name,
        bucket=bucket_name,
        policy=policy_to_add,
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.s3.bucket_policy.absent(
        ctx,
        name=after.get("Name"),
        bucket=aws_s3_bucket.get("name"),
        resource_id=after.get("resource_id"),
    )

    assert ret["comment"]
    assert ret.get("old_state")


@pytest.mark.localstack(
    False,
    "To skip running on localstack as anomaly_monitor is only working with real aws",
)
@pytest_asyncio.fixture(scope="module")
async def aws_anomaly_monitor(hub, ctx) -> Dict[str, Any]:
    monitor_name = "idem-fixture-anomaly-monitor-" + str(int(time.time()))
    monitor_type = "CUSTOM"
    monitor_specification = {
        "Dimensions": {"Key": "LINKED_ACCOUNT", "Values": ["123456789101"]}
    }
    ret = await hub.states.aws.costexplorer.anomaly_monitor.present(
        ctx,
        name=monitor_name,
        monitor_name=monitor_name,
        monitor_type=monitor_type,
        monitor_specification=monitor_specification,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.costexplorer.anomaly_monitor.absent(
        ctx, name=monitor_name, resource_id=new_state.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Deleted aws.costexplorer.anomaly_monitor '{monitor_name}'" in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.mark.localstack(
    False,
    "To skip running on localstack as wafv2_web_acl is only working with real aws",
)
@pytest_asyncio.fixture(scope="module")
async def aws_wafv2_web_acl(hub, ctx) -> Dict[str, Any]:
    web_acl_name = "idem-fixture-web-acl-" + str(int(time.time()))
    default_action = {"Allow": {}}
    visibility_config = {
        "CloudWatchMetricsEnabled": False,
        "MetricName": "test-waf",
        "SampledRequestsEnabled": True,
    }
    scope = "REGIONAL"
    web_acl_ret = await hub.states.aws.wafv2.web_acl.present(
        ctx,
        name=web_acl_name,
        default_action=default_action,
        visibility_config=visibility_config,
        scope=scope,
    )
    assert web_acl_ret["result"], web_acl_ret["comment"]
    assert not web_acl_ret.get("old_state") and web_acl_ret.get("new_state")
    new_state = web_acl_ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.wafv2.web_acl.absent(
        ctx,
        name=web_acl_name,
        resource_id=new_state.get("resource_id"),
        scope=scope,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.mark.localstack(
    False, "To skip running on localstack as api_gateway is only working with real aws"
)
@pytest_asyncio.fixture(scope="module")
async def aws_api_gateway(hub, ctx) -> Dict[str, Any]:
    rest_api_name = "idem-fixture-graph-ql-api-" + str(int(time.time()))
    api_gateway_ret = await hub.exec.boto3.client.apigateway.create_rest_api(
        ctx,
        name=rest_api_name,
    )
    assert api_gateway_ret["result"], api_gateway_ret["comment"]

    create_deployment = await hub.exec.boto3.client.apigateway.get_resources(
        ctx,
        restApiId=api_gateway_ret["ret"]["id"],
    )
    items = create_deployment["ret"]["items"]
    for item in items:
        resource_id = item.get("id")

    put_methods = await hub.exec.boto3.client.apigateway.put_method(
        ctx,
        restApiId=api_gateway_ret["ret"]["id"],
        resourceId=resource_id,
        httpMethod="GET",
        authorizationType="NONE",
    )
    assert put_methods["result"], put_methods["comment"]

    method_integration = await hub.exec.boto3.client.apigateway.put_integration(
        ctx,
        restApiId=api_gateway_ret["ret"]["id"],
        resourceId=resource_id,
        httpMethod="GET",
        type="MOCK",
    )
    assert method_integration["result"], method_integration["comment"]

    deploy_api = await hub.exec.boto3.client.apigateway.create_deployment(
        ctx,
        restApiId=api_gateway_ret["ret"]["id"],
    )
    assert deploy_api["result"], deploy_api["comment"]
    deployment_id = deploy_api["ret"]["id"]

    create_stage_1 = await hub.exec.boto3.client.apigateway.create_stage(
        ctx,
        restApiId=api_gateway_ret["ret"]["id"],
        stageName="prod",
        deploymentId=deployment_id,
    )
    assert create_stage_1["result"], create_stage_1["comment"]
    api_gateway_ret["stage"] = create_stage_1["ret"]
    create_stage_2 = await hub.exec.boto3.client.apigateway.create_stage(
        ctx,
        restApiId=api_gateway_ret["ret"]["id"],
        stageName="test",
        deploymentId=deployment_id,
    )
    assert create_stage_2["result"], create_stage_2["comment"]
    api_gateway_ret["stage2"] = create_stage_2["ret"]

    yield api_gateway_ret

    delete_rest_api = await hub.exec.boto3.client.apigateway.delete_rest_api(
        ctx,
        restApiId=api_gateway_ret["ret"]["id"],
    )
    assert delete_rest_api["result"], delete_rest_api["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_launch_configuration(hub, ctx, aws_image) -> Dict[str, Any]:
    """
    Create and cleanup  launch configuration for a module that needs it
    :return: a description of a launch configuration
    """
    launch_configuration_name = "idem-fixture-launch-config-" + str(uuid.uuid4())
    ret = await hub.states.aws.autoscaling.launch_configuration.present(
        ctx,
        name=launch_configuration_name,
        instance_type="t2.micro",
        image_id=aws_image,
    )
    assert ret["result"], ret["comment"]
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.autoscaling.launch_configuration.absent(
        ctx,
        name=launch_configuration_name,
        resource_id=ret.get("new_state").get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_launch_template(hub, ctx, aws_image) -> Dict[str, Any]:
    """
    Create and cleanup  launch template for a module that needs it
    :return: a description of a launch template
    """
    launch_template_name = "idem-fixture-launch-template-" + str(uuid.uuid4())
    ret = await hub.states.aws.ec2.launch_template.present(
        ctx,
        name=launch_template_name,
        version_description="test description",
        launch_template_data={
            "ImageId": aws_image,
            "UserData": "#!/bin/bash -xe",
            "InstanceType": "m5.large",
        },
    )
    assert ret["result"], ret["comment"]
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.launch_template.absent(
        ctx,
        name=launch_template_name,
        resource_id=ret.get("new_state").get("resource_id"),
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="function")
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def aws_test_ecr_repository(hub, ctx):
    """
    This is scoped for each function as the test function can decide what to do with this test repository
    and do not introduce inter test dependency.
    """
    test_repository_name = "idem-test-repository-" + str(uuid.uuid4())
    await hub.exec.boto3.client.ecr.create_repository(
        ctx,
        registryId=None,
        repositoryName=test_repository_name,
        imageTagMutability="IMMUTABLE",
        imageScanningConfiguration={"scanOnPush": True},
        encryptionConfiguration={"encryptionType": "AES256"},
        tags=hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            {"Name": test_repository_name}
        ),
    )
    yield test_repository_name
    try:
        await hub.exec.boto3.client.ecr.delete_repository(
            ctx, repositoryName=test_repository_name, registryId=None, force=True
        )
    except ClientError:
        # Best effort to delete but any exception should not fail the test (although manual cleanup will be required)
        pass


@pytest_asyncio.fixture(scope="function")
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def aws_test_ecr_repository_with_policy(hub, ctx):
    """
    This is scoped for each function as the test function can decide what to do with this test repository with policy
    and do not introduce inter test dependency.
    """
    test_repository_name = "idem-test-repository-with-policy-" + str(uuid.uuid4())
    await hub.exec.boto3.client.ecr.create_repository(
        ctx,
        registryId=None,
        repositoryName=test_repository_name,
        imageTagMutability="IMMUTABLE",
        imageScanningConfiguration={"scanOnPush": True},
        encryptionConfiguration={"encryptionType": "AES256"},
        tags=hub.tool.aws.tag_utils.convert_tag_dict_to_list(
            {"Name": test_repository_name}
        ),
    )

    policy_text = {
        "Statement": [
            {
                "Action": "ecr:DescribeImages",
                "Effect": "Allow",
                "Principal": {"Service": "ecs.amazonaws.com"},
                "Sid": str(uuid.uuid4()),
            }
        ],
        "Version": "2012-10-17",
    }
    await hub.exec.boto3.client.ecr.set_repository_policy(
        ctx,
        registryId=None,
        repositoryName=test_repository_name,
        policyText=hub.tool.aws.state_comparison_utils.standardise_json(policy_text),
        force=True,
    )
    yield test_repository_name
    try:
        await hub.exec.boto3.client.ecr.delete_repository(
            ctx, repositoryName=test_repository_name, registryId=None, force=True
        )
    except ClientError:
        # Best effort to delete but any exception should not fail the test (although manual cleanup will be required)
        pass


@pytest.mark.localstack(
    False,
    "To skip running on localstack as ecr_repository is only working with real aws",
)
@pytest_asyncio.fixture(scope="module")
async def aws_ecr_repository(hub, ctx) -> Dict[str, Any]:
    repository_name = "idem-fixture-ecr-repository-" + str(int(time.time()))
    present_ret = await hub.states.aws.ecr.repository.present(
        ctx,
        name=repository_name,
    )
    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.ecr.repository.absent(
        ctx, name=repository_name, resource_id=new_state.get("resource_id")
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret.get("old_state") and not absent_ret.get("new_state")


@pytest.mark.localstack(
    False,
    "To skip running on localstack as ecr_image is only working with real aws",
)
@pytest_asyncio.fixture(scope="module")
async def aws_ecr_image(hub, ctx, aws_ecr_repository) -> Dict[str, Any]:
    image_tag = "idem-test-" + str(int(time.time()))
    registry_id = aws_ecr_repository.get("registry_id")
    repository_name = aws_ecr_repository.get("repository_name")

    # Login into ECR registry
    get_authorization_token_ret = (
        await hub.exec.boto3.client.ecr.get_authorization_token(
            ctx,
            registryIds=[registry_id],
        )
    )
    assert get_authorization_token_ret["result"], get_authorization_token_ret["comment"]
    assert get_authorization_token_ret["ret"]
    authorization_data = get_authorization_token_ret["ret"]["authorizationData"][0]
    ecr_username = "AWS"
    ecr_password = (
        base64.b64decode(authorization_data["authorizationToken"])
        .replace(b"AWS:", b"")
        .decode("utf-8")
    )
    ecr_url = authorization_data["proxyEndpoint"].replace("https://", "")
    docker_client = docker.from_env()
    docker_client.login(
        username=ecr_username, password=ecr_password, registry=ecr_url, reauth=True
    )

    # Build local image
    image, _ = docker_client.images.build(
        fileobj=io.BytesIO(b"FROM alpine\n" b"CMD echo Hello Idem!")
    )

    # Push image to ECR registry
    ecr_repository_name = f"{ecr_url}/{repository_name}"
    image.tag(ecr_repository_name, tag=image_tag)
    docker_client.images.push(repository=ecr_repository_name, tag=image_tag)

    # Get image data from ECR registry
    describe_images_ret = await hub.exec.boto3.client.ecr.describe_images(
        ctx,
        registryId=registry_id,
        repositoryName=repository_name,
        imageIds=[{"imageTag": image_tag}],
    )
    assert describe_images_ret["result"], describe_images_ret["comment"]
    assert describe_images_ret["ret"]
    image_data = describe_images_ret["ret"]["imageDetails"][0]

    # yield
    ecr_image = {
        "registry_id": registry_id,
        "repository_name": repository_name,
        "image_digest": image_data["imageDigest"],
        "image_tags": image_data["imageTags"],
        "image_size_in_bytes": image_data["imageSizeInBytes"],
        "image_manifest_media_type": image_data["imageManifestMediaType"],
        "artifact_media_type": image_data["artifactMediaType"],
    }
    yield ecr_image

    # Delete image from ECR registry
    delete_ret = await hub.exec.boto3.client.ecr.batch_delete_image(
        ctx,
        registryId=registry_id,
        repositoryName=repository_name,
        imageIds=[
            {
                "imageDigest": image_data["imageDigest"],
                "imageTag": image_tag,
            },
        ],
    )
    assert delete_ret["result"], delete_ret["comment"]
    assert delete_ret["ret"]

    # Delete local image
    image.remove(force=True)


@pytest.mark.localstack(
    False,
    "To skip running on localstack as certificate_manager with email is only working with real aws",
)
@pytest_asyncio.fixture(scope="module")
async def aws_certificate_manager_email(hub, ctx) -> Dict[str, Any]:
    name = "idem-fixture-acm" + str(time.time())
    domain_name = name + ".example.com"
    validation_method = "EMAIL"
    subject_alternative_names = [name + ".example.net"]
    options = {"CertificateTransparencyLoggingPreference": "ENABLED"}
    tags = [{"Key": "Name", "Value": name}]
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx,
        name=name,
        domain_name=domain_name,
        validation_method=validation_method,
        subject_alternative_names=subject_alternative_names,
        domain_validation_options=[
            {"DomainName": domain_name, "ValidationDomain": "example.com"},
        ],
        options=options,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    new_state = ret.get("new_state")
    resource_id = new_state.get("resource_id")

    yield new_state

    ret = await hub.states.aws.acm.certificate_manager.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.acm.certificate_manager '{resource_id}'" in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]


@pytest_asyncio.fixture(scope="module")
async def aws_certificate_manager_fqdns(hub, ctx) -> Dict[str, Any]:
    # Request 'edu' certificate
    name = "idem-fixture-acm" + str(uuid.uuid4())
    domain_name = name + ".edu.com"
    validation_method = "DNS"
    subject_alternative_names = ["testing.edu.com"]
    options = {"CertificateTransparencyLoggingPreference": "ENABLED"}
    tags = [{"Key": "Name", "Value": name}]
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx,
        name=name,
        domain_name=domain_name,
        validation_method=validation_method,
        subject_alternative_names=subject_alternative_names,
        domain_validation_options=[
            {"DomainName": domain_name, "ValidationDomain": "edu.com"},
        ],
        options=options,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    new_state = ret.get("new_state")
    resource_id = new_state.get("resource_id")

    yield new_state

    ret = await hub.states.aws.acm.certificate_manager.absent(
        ctx, name=resource_id, resource_id=resource_id
    )

    assert ret["result"], ret["comment"]
    assert f"Deleted aws.acm.certificate_manager '{resource_id}'" in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]


@pytest_asyncio.fixture(scope="module")
async def aws_certificate_manager_import(hub, ctx) -> Dict[str, Any]:
    name = "idem-fixture-acm" + str(uuid.uuid4())
    domain_name = name + ".example.com"
    tags = [{"Key": "Name", "Value": name}]
    # Generating Private_Key and Certificate to test importing of certificate
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)

    # Create a self-signed certificate
    validity = 10 * 365 * 24 * 60 * 60
    cert = crypto.X509()
    cert.get_subject().C = "UK"
    cert.get_subject().ST = "London"
    cert.get_subject().L = "London"
    cert.get_subject().O = "Dummy Company Ltd"
    cert.get_subject().OU = "Dummy Company Ltd"
    cert.get_subject().CN = domain_name
    cert.set_serial_number(1000)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(validity)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, "sha1")

    # Import Certificate
    certificate_waiter = hub.tool.boto3.custom_waiter.waiter_wrapper(
        name="CertificateNotInUse",
        operation="DescribeCertificate",
        argument=["Certificate.InUseBy"],
        acceptors=[
            {
                "matcher": "path",
                "expected": True,
                "state": "success",
                "argument": "length(Certificate.InUseBy) == `0`",
            },
            {
                "matcher": "path",
                "expected": False,
                "state": "retry",
                "argument": "length(Certificate.InUseBy) > `0`",
            },
        ],
        client=await hub.tool.boto3.client.get_client(ctx, "acm"),
    )
    ret = await hub.states.aws.acm.certificate_manager.present(
        ctx=ctx,
        name=name,
        private_key=crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode("utf-8"),
        certificate=crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode("utf-8"),
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    new_state = ret.get("new_state")
    resource_id = new_state.get("resource_id")

    yield new_state

    # waiter configuration to wait until resources that are using the certificate are deleted and their status
    # is propagated to the certificate manager
    before = await hub.exec.boto3.client.acm.describe_certificate(
        ctx, CertificateArn=resource_id
    )
    await hub.tool.boto3.client.wait(
        ctx,
        "acm",
        "CertificateNotInUse",
        certificate_waiter,
        CertificateArn=resource_id,
        WaiterConfig=hub.tool.aws.waiter_utils.create_waiter_config(
            default_delay=18,
            default_max_attempts=80,
            timeout_config=None,
        ),
    )

    ret = await hub.states.aws.acm.certificate_manager.absent(
        ctx, name=resource_id, resource_id=resource_id
    )

    assert ret["result"], ret["comment"]
    assert f"Deleted aws.acm.certificate_manager '{resource_id}'" in ret["comment"]
    assert ret["old_state"] and not ret["new_state"]


@pytest_asyncio.fixture(scope="module")
async def aws_apigateway_domain_name(
    hub, ctx, aws_certificate_manager_import
) -> Dict[str, Any]:
    # domain_name fixture for testing associated resources such as base path mapping
    name = aws_certificate_manager_import["domain_name"]
    regional_certificate_arn = aws_certificate_manager_import["resource_id"]
    security_policy = "TLS_1_2"
    endpoint_configuration = {
        "types": ["REGIONAL"],
    }
    present_ret = await hub.states.aws.apigateway.domain_name.present(
        ctx,
        name=name,
        regional_certificate_arn=regional_certificate_arn,
        security_policy=security_policy,
        endpoint_configuration=endpoint_configuration,
    )
    new_state = present_ret.get("new_state")
    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigateway.domain_name.absent(
        ctx,
        name=name,
        resource_id=new_state["resource_id"],
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret.get("old_state") and not absent_ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_apigatewayv2_api(hub, ctx) -> Dict[str, Any]:
    api_name = "idem-fixture-apigatewayv2-api-" + str(int(time.time()))

    present_ret = await hub.states.aws.apigatewayv2.api.present(
        ctx,
        name=api_name,
        protocol_type="WEBSOCKET",
        route_selection_expression="${request.method} ${request.path}",
    )
    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigatewayv2.api.absent(
        ctx, name=api_name, resource_id=new_state.get("resource_id")
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret.get("old_state") and not absent_ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_apigateway_rest_api(hub, ctx) -> Dict[str, Any]:
    api_name = "idem-fixture-apigateway-rest-api" + str(uuid.uuid4())

    present_ret = await hub.states.aws.apigateway.rest_api.present(
        ctx,
        name=api_name,
    )

    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigateway.rest_api.absent(
        ctx, name=api_name, resource_id=new_state.get("resource_id")
    )
    await asyncio.sleep(30)
    assert absent_ret["result"], absent_ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_apigateway_method(hub, ctx, aws_apigateway_rest_api) -> Dict[str, Any]:
    method_name = "idem-fixture-apigateway-method" + str(uuid.uuid4())
    http_method = "GET"
    authorization_type = "NONE"
    rest_api_id = aws_apigateway_rest_api.get("resource_id")
    resources = await hub.exec.boto3.client.apigateway.get_resources(
        ctx, restApiId=rest_api_id
    )
    parent_resource_id = resources["ret"]["items"][0].get("id")

    resource_id = f"{rest_api_id}-{parent_resource_id}-{http_method}"
    response_parameters = {"method.request.header.custom-header": True}

    present_ret = await hub.states.aws.apigateway.method.present(
        ctx,
        name=method_name,
        rest_api_id=rest_api_id,
        parent_resource_id=parent_resource_id,
        http_method=http_method,
        authorization_type=authorization_type,
        request_parameters=response_parameters,
    )

    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigateway.method.absent(
        ctx,
        name=method_name,
        rest_api_id=rest_api_id,
        parent_resource_id=parent_resource_id,
        http_method=http_method,
        resource_id=resource_id,
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret.get("old_state") and not absent_ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_apigateway_method_response(
    hub, ctx, aws_apigateway_method
) -> Dict[str, Any]:
    method_response_name = "idem-fixture-apigateway-method-response" + str(uuid.uuid4())
    rest_api_id = aws_apigateway_method.get("rest_api_id")
    parent_resource_id = aws_apigateway_method.get("parent_resource_id")
    http_method = aws_apigateway_method.get("http_method")
    status_code = "400"
    resource_id = f"{rest_api_id}-{parent_resource_id}-{http_method}-{status_code}"

    present_ret = await hub.states.aws.apigateway.method_response.present(
        ctx,
        name=method_response_name,
        rest_api_id=rest_api_id,
        http_method=http_method,
        status_code=status_code,
        parent_resource_id=parent_resource_id,
    )

    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigateway.method_response.absent(
        ctx,
        name=method_response_name,
        resource_id=resource_id,
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret.get("old_state") and not absent_ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_apigateway_integration(hub, ctx, aws_apigateway_method) -> Dict[str, Any]:
    integration_name = "idem-fixture-apigateway-integration" + str(int(time.time()))
    rest_api_id = aws_apigateway_method.get("rest_api_id")
    parent_resource_id = aws_apigateway_method.get("parent_resource_id")
    http_method = aws_apigateway_method.get("http_method")
    input_type = "MOCK"

    resource_id = f"{rest_api_id}-{parent_resource_id}-{http_method}"

    present_ret = await hub.states.aws.apigateway.integration.present(
        ctx,
        name=integration_name,
        rest_api_id=rest_api_id,
        parent_resource_id=parent_resource_id,
        http_method=http_method,
        input_type=input_type,
    )
    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigateway.integration.absent(
        ctx,
        name=integration_name,
        resource_id=resource_id,
    )
    assert absent_ret["result"], absent_ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_apigateway_deployment(
    hub, ctx, aws_apigateway_integration
) -> Dict[str, Any]:
    rest_api_id = aws_apigateway_integration.get("rest_api_id")
    deployment_name = "idem-fixture-apigateway-deployment" + str(int(time.time()))
    present_ret = await hub.states.aws.apigateway.deployment.present(
        ctx,
        name=deployment_name,
        rest_api_id=rest_api_id,
    )
    resource_id = present_ret.get("new_state").get("resource_id")
    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigateway.deployment.absent(
        ctx,
        name=deployment_name,
        rest_api_id=rest_api_id,
        resource_id=resource_id,
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret.get("old_state") and not absent_ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_apigatewayv2_route(hub, ctx, aws_apigatewayv2_api) -> Dict[str, Any]:
    route_name = "idem-fixture-apigatewayv2-route-" + str(uuid.uuid4())
    api_id = aws_apigatewayv2_api.get("api_id")

    present_ret = await hub.states.aws.apigatewayv2.route.present(
        ctx, name=route_name, api_id=api_id, route_key="$default"
    )
    assert present_ret["result"], present_ret["comment"]
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    new_state = present_ret.get("new_state")

    yield new_state

    absent_ret = await hub.states.aws.apigatewayv2.route.absent(
        ctx, name=route_name, api_id=api_id, resource_id=new_state.get("resource_id")
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret.get("old_state") and not absent_ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_docdb_subnet_group(hub, ctx, aws_ec2_subnet) -> Dict[str, Any]:
    """
    Create and cleanup an docdb subnet group for a module that needs it
    :return: a description of an docdb subnet group
    """
    db_subnet_group_name = "idem-fixture-vpc-" + str(uuid.uuid4())
    ret = await hub.states.aws.docdb.db_subnet_group.present(
        ctx,
        name=db_subnet_group_name,
        tags=[{"Key": "Name", "Value": db_subnet_group_name}],
        db_subnet_group_description="db subnet group for testing",
        subnet_ids=[aws_ec2_subnet.get("SubnetId")],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    after.get("resource_id")

    yield after

    ret = await hub.states.aws.docdb.db_subnet_group.absent(
        ctx, name=db_subnet_group_name, resource_id=db_subnet_group_name
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_image(hub, ctx) -> str:
    """
    Fetch ami image id
    :return: image id
    """
    image_id = "ami-73949613"
    if not hub.tool.utils.is_running_localstack(ctx):
        filters = [
            {"Name": "owner-alias", "Values": ["amazon"]},
            {"Name": "image-type", "Values": ["machine"]},
            {"Name": "architecture", "Values": ["x86_64"]},
            {"Name": "platform", "Values": ["windows"]},
            {"Name": "state", "Values": ["available"]},
            {"Name": "virtualization-type", "Values": ["hvm"]},
        ]
        resource = await hub.exec.boto3.client.ec2.describe_images(
            ctx,
            Filters=filters,
            IncludeDeprecated=False,
            DryRun=False,
        )

        image_id = resource.ret["Images"][0]["ImageId"]

    yield image_id


@pytest_asyncio.fixture(scope="module")
async def aws_hosted_zone(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup a Hosted zone for a module that needs it.
    :return: Description of the Hosted zone.
    """
    hosted_zone_name = f"idem-fixture-hosted-zone-{str(uuid.uuid4())}.com"
    ret = await hub.exec.boto3.client.route53.create_hosted_zone(
        ctx,
        Name=hosted_zone_name,
        CallerReference=str(time.gmtime()),
    )
    assert ret["result"], ret["comment"]
    aws_hosted_zone = (
        hub.tool.aws.route53.conversion_utils.convert_raw_hosted_zone_to_present(
            raw_resource=ret, idem_resource_name=hosted_zone_name
        )
    )

    yield aws_hosted_zone

    hosted_zone_id = ret["ret"]["HostedZone"]["Id"]
    record_sets = await hub.exec.boto3.client.route53.list_resource_record_sets(
        ctx,
        HostedZoneId=hosted_zone_id,
        MaxItems="20",
    )
    assert record_sets["result"], record_sets["comment"]

    for record in record_sets["ret"]["ResourceRecordSets"]:
        if record.get("Type") in ["NS", "SOA"]:
            # These record types can not be deleted but does not prevent from deleting hosted zone
            continue
        idem_record = hub.tool.aws.route53.conversion_utils.convert_raw_resource_record_to_present(
            hosted_zone_id=hosted_zone_id, raw_resource=record
        )
        change_batch = (
            hub.tool.aws.route53.resource_record_utils.create_change_batch_for_update(
                "DELETE", idem_record
            )
        )
        response = await hub.exec.boto3.client.route53.change_resource_record_sets(
            ctx, HostedZoneId=hosted_zone_id, ChangeBatch=change_batch
        )
        assert response["result"], response["comment"]

    ret = await hub.exec.boto3.client.route53.delete_hosted_zone(ctx, Id=hosted_zone_id)
    assert ret["result"], ret["comment"]


@pytest.mark.localstack(
    False,
    "To skip running on localstack as budgets.budget with email is only working with real aws",
)
@pytest_asyncio.fixture(scope="module")
async def aws_budget(hub, ctx) -> Dict[str, Any]:
    budget_name = "idem-fixture-budget-" + str(int(time.time()))
    time_unit = "MONTHLY"
    budget_type = "COST"
    budget_limit = {"Amount": "1000.0", "Unit": "USD"}
    cost_filters = {}
    cost_types = {
        "IncludeCredit": False,
        "IncludeDiscount": True,
        "IncludeOtherSubscription": True,
        "IncludeRecurring": True,
        "IncludeRefund": False,
        "IncludeSubscription": True,
        "IncludeSupport": True,
        "IncludeTax": True,
        "IncludeUpfront": True,
        "UseAmortized": False,
        "UseBlended": False,
    }
    time_period = {
        "End": "2023-06-15 05:30:00+05:30",
        "Start": "2022-05-01 05:30:00+05:30",
    }
    notifications_with_subscribers = [
        {
            "Notification": {
                "ComparisonOperator": "GREATER_THAN",
                "NotificationState": "OK",
                "NotificationType": "FORECASTED",
                "Threshold": 90.0,
            },
            "Subscribers": [{"Address": "abc@test.com", "SubscriptionType": "EMAIL"}],
        }
    ]
    ret = await hub.states.aws.budgets.budget.present(
        ctx,
        name=budget_name,
        budget_name=budget_name,
        time_unit=time_unit,
        budget_type=budget_type,
        budget_limit=budget_limit,
        cost_filters=cost_filters,
        cost_types=cost_types,
        time_period=time_period,
        notifications_with_subscribers=notifications_with_subscribers,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.budgets.budget.absent(
        ctx, name=budget_name, resource_id=new_state.get("resource_id")
    )
    assert ret["result"], ret["comment"]
    assert f"Deleted aws.budgets.budget '{budget_name}'" in ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_role_3(hub, ctx) -> Dict[str, Any]:
    role_name = "idem-fixture-role-3-" + str(int(time.time()))
    assume_role_policy_document = '{"Version": "2012-10-17","Statement": [{"Effect": "Allow","Principal": {"Service": "budgets.amazonaws.com"},"Action": "sts:AssumeRole"}]}'
    description = "Idem IAM role for Budget fixture"

    ret = await hub.states.aws.iam.role.present(
        ctx,
        name=role_name,
        assume_role_policy_document=assume_role_policy_document,
        description=description,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    if not hub.tool.utils.is_running_localstack(ctx):
        # TODO: A hard-code sleep is not a good practice. This need to be fixed later with proper waiter implementation.
        await asyncio.sleep(45)

    yield after

    ret = await hub.states.aws.iam.role.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_organization_policy_1(hub, ctx) -> Dict[str, Any]:
    policy_name = "idem-fixture-organizations-policy-" + str(int(time.time()))
    description = (
        "Enables admins of attached accounts to delegate all Budgets permissions"
    )
    policy_type = "SERVICE_CONTROL_POLICY"
    content = """{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": [
            "iam:AttachGroupPolicy",
            "iam:AttachRolePolicy",
            "iam:AttachUserPolicy",
            "iam:DetachGroupPolicy",
            "iam:DetachRolePolicy",
            "iam:DetachUserPolicy",
            "organizations:AttachPolicy",
            "organizations:DetachPolicy"
          ],
          "Resource": "*",
          "Effect": "Allow"
        }
      ]
    }"""

    create_tag = [{"Key": "org", "Value": "idem"}]

    result = await hub.states.aws.organizations.policy.present(
        ctx,
        name=policy_name,
        description=description,
        policy_type=policy_type,
        content=content,
        tags=create_tag,
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.policy.absent(
        ctx,
        name=result.get("new_state").get("name"),
        resource_id=result.get("new_state").get("resource_id"),
    )

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_organization_policy_attachment(
    hub, ctx, aws_organization_unit, aws_organization_policy_1
) -> Dict[str, Any]:
    attach_policy_organization = "idem-test-policy-organization-attachment-" + str(
        uuid.uuid4()
    )

    result = await hub.states.aws.organizations.policy_attachment.present(
        ctx,
        name=attach_policy_organization,
        policy_id=aws_organization_policy_1["resource_id"],
        target_id=aws_organization_unit["resource_id"],
    )

    assert result["result"], result["comment"]
    after = result.get("new_state")

    yield after

    ret = await hub.states.aws.organizations.policy_attachment.absent(
        ctx,
        name=attach_policy_organization,
        policy_id=aws_organization_policy_1["resource_id"],
        target_id=aws_organization_unit["resource_id"],
    )

    assert ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_autoscaling_group(
    hub, ctx, aws_ec2_subnet, aws_launch_configuration
) -> Dict[str, Any]:
    name = "idem-fixture-autoscaling-group-" + str(int(time.time()))
    launch_config_name = aws_launch_configuration.get("resource_id")
    sub_net_id = aws_ec2_subnet.get("SubnetId")
    min_size = 1
    max_size = 2
    desired_capacity = 1

    ret = await hub.states.aws.autoscaling.auto_scaling_group.present(
        ctx,
        name=name,
        min_size=min_size,
        max_size=max_size,
        launch_configuration_name=launch_config_name,
        desired_capacity=desired_capacity,
        vpc_zone_identifier=sub_net_id,
        default_cooldown=300,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.autoscaling.auto_scaling_group.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_volume(hub, ctx, aws_availability_zones) -> Dict[str, Any]:
    """
    Create and cleanup an EC2 volume for a module that needs it
    :return: a description of an EC2 volume
    """
    volume_temp_name = "idem-fixture-volume" + str(int(time.time()))
    ret = await hub.states.aws.ec2.volume.present(
        ctx,
        name=volume_temp_name,
        availability_zone=aws_availability_zones[0]["zone_name"],
        size=1,
        tags={"Name": volume_temp_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    for _ in range(10):
        # Wait until the volume is available
        get = await hub.exec.aws.ec2.volume.get(
            ctx,
            name=volume_temp_name,
            resource_id=resource_id,
        )
        if get.ret["state"] == "available":
            break
        else:
            await hub.pop.loop.sleep(10)
    yield after

    ret = await hub.states.aws.ec2.volume.absent(
        ctx, name=volume_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_flow_log(hub, ctx, aws_s3_bucket, aws_ec2_vpc) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 flow_log for a module that needs it
    :return: a description of an ec2 flow_log
    """
    flow_log_temp_name = "idem-fixture-flow_log-" + str(int(time.time()))
    resource_type = "VPC"
    traffic_type = "ALL"
    log_destination_type = "s3"
    log_destination = "arn:aws:s3:::" + aws_s3_bucket.get("name")
    resource_ids = [aws_ec2_vpc.get("VpcId")]
    ret = await hub.states.aws.ec2.flow_log.present(
        ctx,
        name=flow_log_temp_name,
        resource_type=resource_type,
        tags=[{"Key": "Name", "Value": flow_log_temp_name}],
        traffic_type=traffic_type,
        log_destination_type=log_destination_type,
        log_destination=log_destination,
        resource_ids=resource_ids,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.flow_log.absent(
        ctx, name=flow_log_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_cloudwatch_log_group(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an Cloudwatch log_group for a module that needs it
    :return: a description of a cloudwatch log_group
    """
    log_group_temp_name = "idem-fixture-log_group-" + str(int(time.time()))
    tags = {"testKey1": "testValue1"}
    ret = await hub.states.aws.cloudwatch.log_group.present(
        ctx,
        name=log_group_temp_name,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.cloudwatch.log_group.absent(
        ctx, name=log_group_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_iam_group(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an IAM Group for a module that needs it
    :return: a description of an IAM Group
    """
    group_name = "idem-fixture-group-" + str(int(time.time()))
    path = "/"
    ret = await hub.states.aws.iam.group.present(
        ctx, name=group_name, group_name=group_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.iam.group.absent(
        ctx, name=after.get("name"), resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_snapshot(hub, ctx) -> str:
    """
    Create and cleanup an Ec2 snapshot for a module that needs it
    :return: a description of an ec2 snapshot
    """
    snapshot_name = "idem-fixture-snapshot-" + str(int(time.time()))
    volume_name = "idem-fixture-volume-" + str(int(time.time()))
    snapshot_ids = await hub.exec.boto3.client.ec2.describe_snapshots(
        ctx, OwnerIds=["self"]
    )
    snapshot_id = None
    if snapshot_ids["ret"].get("Snapshots"):
        snapshot_id = snapshot_ids["ret"].get("Snapshots")[0].get("SnapshotId")

    new_snapshot_created = False
    if not snapshot_id:
        vol_ret = await hub.states.aws.ec2.volume.present(
            ctx,
            name=volume_name,
            availability_zone=ctx["acct"].get("region_name") + "a",
            encrypted=False,
            size=1,
            volume_type="gp3",
        )
        assert vol_ret["result"], vol_ret["comment"]
        volume_id = vol_ret["ret"].get("VolumeId")

        await hub.tool.boto3.client.wait(
            ctx, "ec2", "volume_available", VolumeIds=[volume_id]
        )

        ret = await hub.states.aws.ec2.snapshot.present(
            ctx, name=snapshot_name, volume_id=volume_id
        )
        assert ret["result"], ret["comment"]
        snapshot_id = ret["ret"]["SnapshotId"]
        await hub.tool.boto3.client.wait(
            ctx, "ec2", "snapshot_completed", SnapshotIds=[snapshot_id]
        )
        new_snapshot_created = True

    yield snapshot_id

    if new_snapshot_created:
        snap_ret = await hub.states.aws.ec2.snapshot.absent(
            ctx, name=snapshot_name, resource_id=snapshot_id
        )
        assert snap_ret["result"], snap_ret["comment"]
        vol_ret = await hub.states.aws.ec2.volume.absent(
            ctx, name=volume_name, volume_id=volume_id
        )
        assert vol_ret["result"], vol_ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_ec2_ami(hub, ctx, aws_snapshot) -> Dict[str, Any]:
    """
    Create and cleanup an Ec2 ami for a module that needs it
    :return: a description of an ec2 ami
    """
    ami_temp_name = "idem-fixture-ami-" + str(int(time.time()))
    root_device_name = "/dev/sda1"

    snapshot_id = aws_snapshot
    block_device_mappings = [
        {
            "DeviceName": "/dev/sda1",
            "Ebs": {
                "DeleteOnTermination": False,
                "SnapshotId": f"{snapshot_id}",
                "VolumeSize": 8,
                "VolumeType": "gp2",
            },
        }
    ]

    tags = {"idem-test-ami-key": "idem-test-ami-value"}

    ret = await hub.states.aws.ec2.ami.present(
        ctx,
        name=ami_temp_name,
        block_device_mappings=block_device_mappings,
        root_device_name=root_device_name,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.ec2.ami.absent(
        ctx, name=ami_temp_name, resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_s3_bucket_lifecycle(hub, ctx, aws_s3_bucket) -> Dict[str, Any]:
    """
    Create and cleanup a s3 bucket lifecycle for a module that needs it
    :return: a description of a s3 bucket lifecycle
    """
    bucket_lifecycle_name = "idem-fixture-s3-bucket-lifecycle-" + str(int(time.time()))
    lifecycle_configuration = {
        "Rules": [
            {
                "ID": "rule-1",
                "Filter": {},
                "Status": "Enabled",
                "NoncurrentVersionTransitions": [
                    {
                        "NoncurrentDays": 31,
                        "StorageClass": "STANDARD_IA",
                        "NewerNoncurrentVersions": 31,
                    }
                ],
                "NoncurrentVersionExpiration": {
                    "NoncurrentDays": 60,
                    "NewerNoncurrentVersions": 60,
                },
                "AbortIncompleteMultipartUpload": {"DaysAfterInitiation": 5},
            }
        ]
    }
    bucket_name = aws_s3_bucket.get("name")
    ret = await hub.states.aws.s3.bucket_lifecycle.present(
        ctx,
        name=bucket_lifecycle_name,
        bucket=bucket_name,
        lifecycle_configuration=lifecycle_configuration,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.s3.bucket_lifecycle.absent(
        ctx, name=after.get("name"), resource_id=after.get("resource_id")
    )
    assert ret["result"], ret["comment"]


@pytest_asyncio.fixture(scope="module")
async def aws_sns_subscription(hub, ctx, aws_sns_topic) -> Dict[str, Any]:
    subscription_name = "idem-fixture-subscription-" + str(uuid.uuid4())
    protocol = "sms"
    endpoint = "+1234"
    topic_arn = aws_sns_topic.get("resource_id")
    return_subscription_arn = True
    delivery_policy = json.dumps(
        {
            "healthyRetryPolicy": {
                "minDelayTarget": 10,
                "maxDelayTarget": 30,
                "numRetries": 10,
                "numMaxDelayRetries": 7,
                "numNoDelayRetries": 0,
                "numMinDelayRetries": 3,
                "backoffFunction": "linear",
            },
            "sicklyRetryPolicy": None,
            "throttlePolicy": None,
            "guaranteed": False,
        },
        separators=(",", ":"),
    )
    attributes = {"DeliveryPolicy": delivery_policy}

    ret = await hub.states.aws.sns.subscription.present(
        ctx,
        name=subscription_name,
        topic_arn=topic_arn,
        protocol=protocol,
        endpoint=endpoint,
        attributes=attributes,
        return_subscription_arn=return_subscription_arn,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    new_state = ret.get("new_state")

    yield new_state

    ret = await hub.states.aws.sns.topic.absent(
        ctx, name=subscription_name, resource_id=new_state.get("resource_id")
    )
    assert ret["result"], ret["comment"]


# ================================================================================
# resource helpers
# ================================================================================
@pytest.fixture(scope="module", name="instance_name")
def aws_instance_name() -> str:
    yield "test-idem-cloud-" + "".join(
        random.choice(string.ascii_lowercase + string.digits) for _ in range(20)
    )


@pytest.fixture(scope="module")
def ssh_key_pair() -> Tuple[ByteString, ByteString]:
    key_pair = RSA.generate(1024)
    yield key_pair.export_key(), key_pair.publickey().export_key()


@pytest.fixture(scope="module")
def zip_file() -> ByteString:
    """
    Create a zip file for tests that use lambda functions
    """
    with tempfile.TemporaryDirectory() as tempdir:
        path = pathlib.Path(tempdir)
        z_path = path.joinpath("code.zip")

        with zipfile.ZipFile(z_path, "w") as myzip:
            myzip.writestr("code.py", "def main():\n\treturn 0")

        with open(z_path, "rb") as fh:
            yield fh.read()


@pytest_asyncio.fixture(scope="function")
async def sns_topic(hub, ctx):
    """
    sns topic arn fixture that cleans up after itself
    """
    name = f"idem-fixture-sns-{str(int(time.time()))}"
    ret = await hub.exec.boto3.client.sns.create_topic(ctx, Name=name)
    assert ret["result"], ret["comment"]
    topic = ret["ret"]
    yield topic
    await hub.exec.boto3.client.sns.delete_topic(ctx, TopicArn=topic["TopicArn"])


@pytest_asyncio.fixture(scope="function")
async def ses_configuration_set(hub, ctx):
    """
    ses config set fixture that cleans up after itself.
    """
    name = f"idem-fixture-ses-config-set-{str(int(time.time()))}"
    ret = await hub.exec.boto3.client.sesv2.create_configuration_set(
        ctx, ConfigurationSetName=name
    )
    assert ret["result"], ret["comment"]
    yield name
    # Also deletes event destinations, no need for explicit deletions of event destinations
    await hub.exec.boto3.client.sesv2.delete_configuration_set(
        ctx, ConfigurationSetName=name
    )


@pytest_asyncio.fixture(scope="module")
async def aws_guardduty_detector(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an AWS Guardduty detector for a module that needs it
    """
    detector_name = "test_detector"
    ret = await hub.states.aws.guardduty.detector.present(ctx, name=detector_name)
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.guardduty.detector.absent(
        ctx, name=after["name"], resource_id=after["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_availability_zones(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.aws.ec2.availability_zones.list(ctx)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)

    yield ret["ret"]


@pytest_asyncio.fixture(scope="module")
async def aws_backup_vault(hub, ctx, aws_kms_key) -> Dict[str, Any]:
    """
    Create and cleanup an backup vault for a module that needs it
    :return: a description of an backup vault
    """
    volume_temp_name = "idem-fixture-volume" + str(int(time.time()))
    tags = {"Key": "resource", "Value": "backup_vault"}
    ret = await hub.states.aws.backup.backup_vault.present(
        ctx, name=volume_temp_name, encryption_key_arn=aws_kms_key["arn"], tags=tags
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    resource_id = after.get("resource_id")

    yield after

    ret = await hub.states.aws.backup.backup_vault.absent(
        ctx, name=volume_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_ses_domain_identity(hub, ctx) -> Dict[str, Any]:
    """
    Create and cleanup an SES domain identity for a module that needs it
    :return: a description of an SES domain identity
    """
    name = "idem-fixture-domain_id-" + str(uuid.uuid4())
    domain = "edu.com"
    ret = await hub.states.aws.ses.domain_identity.present(
        ctx,
        name=name,
        domain=domain,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")
    after.get("resource_id")

    yield after

    ret = await hub.states.aws.ses.domain_identity.absent(
        ctx,
        name=name,
        resource_id=domain,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest_asyncio.fixture(scope="module")
async def aws_guardduty_organization_admin_account(
    hub,
    ctx,
) -> Dict[str, Any]:
    """
    Enable and disable organization_admin_account for AWS Guardduty
    """
    accounts_ret = await hub.exec.boto3.client.organizations.list_accounts(ctx)
    assert accounts_ret["result"], accounts_ret["comment"]
    accounts = accounts_ret["ret"]["Accounts"]
    account_id = accounts[0]["Id"]
    name = "test organization admin account"

    ret = await hub.states.aws.guardduty.organization_admin_account.present(
        ctx, name=name, resource_id=account_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    after = ret.get("new_state")

    yield after

    ret = await hub.states.aws.guardduty.organization_admin_account.absent(
        ctx,
        name=name,
        resource_id=after["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
