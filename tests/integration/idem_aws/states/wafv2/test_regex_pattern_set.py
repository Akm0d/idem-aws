import copy
import time
import uuid
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-regex-pattern-set" + str(int(time.time())),
    "scope": "REGIONAL",
    "description": "Idem test for regex pattern set",
    "regular_expression_list": [{"RegexString": "^(https|http)"}],
    "tags": {"Name": "idem-test-regex-pattern-set" + str(int(time.time()))},
}

# This tests cannot be run on localstack as it throws not supported error during regex pattern set creation
# ClientError: An error occurred (500) when calling the CreateRegexPatternSet operation (reached max retries: 4):
@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test

    ret = await hub.states.aws.wafv2.regex_pattern_set.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.wafv2.regex_pattern_set", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.wafv2.regex_pattern_set", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]
    assert_wafv2_regex_pattern_set(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.wafv2.regex_pattern_set.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.wafv2.regex_pattern_set.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.wafv2.regex_pattern_set.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert_wafv2_regex_pattern_set(described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-resource_id", depends=["present"])
async def test_exec_get_by_resource_id(hub, ctx):
    ret = await hub.exec.aws.wafv2.regex_pattern_set.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        scope=PARAMETER["scope"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert_wafv2_regex_pattern_set(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-list-by-scope", depends=["exec-get-by-resource_id"])
async def test_exec_list_by_resource_id(hub, ctx):
    ret = await hub.exec.aws.wafv2.regex_pattern_set.list(
        ctx,
        name=PARAMETER["name"],
        scope=PARAMETER["scope"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) > 0
    resource = None
    # Find the created resource by resource_id
    for regex_pattern_set in ret["ret"]:
        if regex_pattern_set["resource_id"] == PARAMETER["resource_id"]:
            resource = regex_pattern_set
            break
    assert resource
    assert_wafv2_regex_pattern_set(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(
    name="exec-get-by-resource_id-cloudfront", depends=["exec-list-by-scope"]
)
async def test_get_invalid_resource_id_cloudfront(hub, ctx):
    if ctx["acct"].get("region_name") != "us-east-1":
        ret = await hub.exec.aws.wafv2.regex_pattern_set.get(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            scope="CLOUDFRONT",
        )
        assert not ret["result"], ret["comment"]
        assert ret["ret"] is None
        assert (
            "CLOUDFRONT scope is available only in US East (N. Virginia) Region, us-east-1"
            in str(ret["comment"])
        )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_invalid_cloudfront")
async def test_invalid_present_cloudfront(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["scope"] = "CLOUDFRONT"
    ret = await hub.states.aws.wafv2.regex_pattern_set.present(
        ctx,
        **new_parameter,
    )
    assert not ret["result"], ret["comment"]
    assert ret["old_state"] is None, ret["new_state"] is None
    assert (
        "CLOUDFRONT scope is available only in US East (N. Virginia) Region, us-east-1"
        in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_get_invalid_resource_id(hub, ctx, __test):
    ret = await hub.exec.aws.wafv2.regex_pattern_set.get(
        ctx,
        name=PARAMETER["name"],
        resource_id="77c56053-c72f-45c2-aa25-2f578087976e",
        scope=PARAMETER["scope"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.wafv2.regex_pattern_set", name=PARAMETER["name"]
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update", depends=["exec-get-by-resource_id-cloudfront"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["regular_expression_list"] = [
        {"RegexString": "^(https|http)"},
        {"RegexString": "^(rttps|rttp)"},
    ]
    new_parameter["description"] = "Updated description"
    new_parameter["tags"] = {
        "Name": "idem-test-regex-pattern-set" + str(int(time.time())),
        "scope": new_parameter["scope"],
    }
    ret = await hub.states.aws.wafv2.regex_pattern_set.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.wafv2.regex_pattern_set", name=new_parameter["name"]
            )[0]
            in ret["comment"]
        )
    old_resource = ret["old_state"]
    assert_wafv2_regex_pattern_set(old_resource, PARAMETER)
    resource = ret["new_state"]
    assert_wafv2_regex_pattern_set(resource, new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.wafv2.regex_pattern_set.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        scope=PARAMETER["scope"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert_wafv2_regex_pattern_set(old_resource, PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.wafv2.regex_pattern_set", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.wafv2.regex_pattern_set", name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.wafv2.regex_pattern_set.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        scope=PARAMETER["scope"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.wafv2.regex_pattern_set", name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_regex_pattern_set_absent_with_none_resource_id(hub, ctx):
    regex_pattern_set_temp_name = "idem-test--regex-pattern-" + str(uuid.uuid4())
    # Delete regex-pattern_set with resource_id as None. Result in no-op.
    ret = await hub.states.aws.wafv2.regex_pattern_set.absent(
        ctx,
        name=regex_pattern_set_temp_name,
        resource_id=None,
        scope=PARAMETER["scope"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.wafv2.regex_pattern_set",
            name=regex_pattern_set_temp_name,
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.wafv2.regex_pattern_set.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_wafv2_regex_pattern_set(resource, parameters):
    assert parameters.get("name") == resource.get("name")
    assert parameters.get("scope") == resource.get("scope")
    assert parameters.get("regular_expression_list") == resource.get(
        "regular_expression_list"
    )
    assert parameters.get("description") == resource.get("description")
    assert parameters.get("tags") == resource.get("tags")
