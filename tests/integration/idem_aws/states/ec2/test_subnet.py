import copy
import time
import uuid
from collections import ChainMap

import pytest
import pytest_asyncio


PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-subnet-" + str(int(time.time())),
    "private_dns_name_options_on_launch": {
        "EnableResourceNameDnsAAAARecord": False,
        "EnableResourceNameDnsARecord": False,
        "HostnameType": "resource-name",
    },
    "map_public_ip_on_launch": True,
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_ec2_vpc, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["cidr_block"] = hub.tool.utils.get_sub_cidr_block(
        aws_ec2_vpc.get("CidrBlock"), 28
    )
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    PARAMETER["vpc_id"] = aws_ec2_vpc.get("VpcId")
    ret = await hub.states.aws.ec2.subnet.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert f"Would create aws.ec2.subnet '{PARAMETER['name']}'" in ret["comment"]
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert f"Created aws.ec2.subnet '{PARAMETER['name']}'" in ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["vpc_id"] == resource.get("vpc_id")
    assert PARAMETER["cidr_block"] == resource.get("cidr_block")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["map_public_ip_on_launch"] == resource.get(
        "map_public_ip_on_launch"
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["private_dns_name_options_on_launch"] == resource.get(
            "private_dns_name_options_on_launch"
        )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.subnet.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.ec2.subnet.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.ec2.subnet.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["map_public_ip_on_launch"] == described_resource_map.get(
        "map_public_ip_on_launch"
    )
    assert PARAMETER["tags"] == described_resource_map.get("tags")
    assert PARAMETER["cidr_block"] == described_resource_map.get("cidr_block")
    if not hub.tool.utils.is_running_localstack(ctx):
        assert (
            PARAMETER["private_dns_name_options_on_launch"]
            == described_resource_map["private_dns_name_options_on_launch"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["map_public_ip_on_launch"] = False
    new_parameter["tags"].update(
        {
            f"idem-test-key-{str(int(time.time()))}": f"idem-test-value-{str(int(time.time()))}",
        }
    )
    new_parameter["private_dns_name_options_on_launch"] = {
        "EnableResourceNameDnsAAAARecord": False,
        "EnableResourceNameDnsARecord": False,
        "HostnameType": "resource-name",
    }
    ipv6_cidr_block = None
    if hub.tool.utils.is_running_localstack(ctx):
        # Only try to associate ipv6 block with localstack, since the vpc on real aws does not have an ipv6 block
        ipv6_cidr_block = "2a05:d01c:74f:7200::/64"
    new_parameter["ipv6_cidr_block"] = ipv6_cidr_block
    ret = await hub.states.aws.ec2.subnet.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["cidr_block"] == old_resource.get("cidr_block")
    assert PARAMETER["vpc_id"] == old_resource.get("vpc_id")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["map_public_ip_on_launch"] == old_resource.get(
        "map_public_ip_on_launch"
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["private_dns_name_options_on_launch"] == old_resource.get(
            "private_dns_name_options_on_launch"
        )
    resource = ret["new_state"]
    assert new_parameter["name"] == resource.get("name")
    assert new_parameter["cidr_block"] == resource.get("cidr_block")
    assert new_parameter["vpc_id"] == resource.get("vpc_id")
    assert new_parameter["tags"] == resource.get("tags")
    assert new_parameter["map_public_ip_on_launch"] == resource.get(
        "map_public_ip_on_launch"
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        assert new_parameter["private_dns_name_options_on_launch"] == resource.get(
            "private_dns_name_options_on_launch"
        )
    else:
        assert new_parameter["ipv6_cidr_block"] == resource.get("ipv6_cidr_block")
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["cidr_block"] == old_resource.get("cidr_block")
    assert PARAMETER["vpc_id"] == old_resource.get("vpc_id")
    assert PARAMETER["tags"] == old_resource.get("tags")
    assert PARAMETER["map_public_ip_on_launch"] == old_resource.get(
        "map_public_ip_on_launch"
    )
    if not hub.tool.utils.is_running_localstack(ctx):
        assert PARAMETER["private_dns_name_options_on_launch"] == old_resource.get(
            "private_dns_name_options_on_launch"
        )
    else:
        assert PARAMETER["ipv6_cidr_block"] == old_resource.get("ipv6_cidr_block")
    if __test:
        assert f"Would delete aws.ec2.subnet '{PARAMETER['name']}'" in ret["comment"]
    else:
        assert f"Deleted aws.ec2.subnet '{PARAMETER['name']}'" in ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.subnet '{PARAMETER['name']}' already absent" in ret["comment"]
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_subnet_absent_with_none_resource_id(hub, ctx):
    subnet_temp_name = "idem-test-subnet-" + str(uuid.uuid4())
    # Delete subnet with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.subnet.absent(
        ctx, name=subnet_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert f"aws.ec2.subnet '{subnet_temp_name}' already absent" in ret["comment"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.subnet.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
