import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_update_monitoring_enabled(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)

    state["monitoring_enabled"] = True

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["new"]["monitoring_enabled"] is True
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]

    await hub.pop.loop.sleep(10)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_reset_monitoring_enabled(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["monitoring_enabled"] = None

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"].get("new", {}).get("monitoring_enabled")
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
