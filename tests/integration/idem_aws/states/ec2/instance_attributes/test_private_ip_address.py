import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_reset_private_ip_address(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["private_ip_address"] = None

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"].get("new", {}).get("private_ip_address")
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
