import copy
import uuid
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-vpc-endpoint" + str(uuid.uuid4()),
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_ec2_vpc, aws_ec2_security_group, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test

    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    PARAMETER["vpc_id"] = aws_ec2_vpc.get("VpcId")
    PARAMETER["region_name"] = ctx.acct.get("region_name", "us-west-1")
    PARAMETER["security_group_ids"] = [aws_ec2_security_group.get("resource_id")]

    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        ctx,
        name=PARAMETER["name"],
        vpc_id=PARAMETER["vpc_id"],
        tags=PARAMETER["tags"],
        service_name=f'com.amazonaws.{PARAMETER["region_name"]}.s3',
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
        security_group_ids=PARAMETER["security_group_ids"],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("name")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        PARAMETER["security_group_ids"], resource.get("security_group_ids")
    )
    if __test:
        assert (
            f"Would create aws.ec2.vpc_endpoint '{PARAMETER['name']}' and attach to vpc '{PARAMETER['vpc_id']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Created aws.ec2.vpc_endpoint: '{PARAMETER['name']}' attached to vpc: '{PARAMETER['vpc_id']}'"
            in ret["comment"]
        )
        PARAMETER["resource_id"] = resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.ec2.vpc_endpoint.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret

    described_resource = describe_ret.get(resource_id).get(
        "aws.ec2.vpc_endpoint.present"
    )

    described_resource_map = dict(ChainMap(*described_resource))

    assert "vpc_id" in described_resource_map
    assert "resource_id" in described_resource_map
    assert "tags" in described_resource_map
    assert "service_name" in described_resource_map
    assert "vpc_endpoint_type" in described_resource_map
    assert "private_dns_enabled" in described_resource_map
    assert "security_group_ids" in described_resource_map


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="add_tag_subnet", depends=["describe"])
async def test_add_tag_subnet(hub, ctx, aws_ec2_subnet, __test):
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    resource_id = PARAMETER["resource_id"]

    new_tag = {"Foo": "Bar"}
    # new_tags = tags
    new_parameter["tags"].update(new_tag)
    subnet_ids = [aws_ec2_subnet.get("SubnetId")]
    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
        vpc_id=PARAMETER["vpc_id"],
        tags=new_parameter["tags"],
        service_name=f"com.amazonaws.{PARAMETER['region_name']}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            f"Would succeed in modifying aws.ec2.vpc_endpoint '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Succeeded modifying aws.ec2.vpc_endpoint '{PARAMETER['name']}'"
            in ret["comment"]
        )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_parameter["tags"] == resource.get("tags")
    assert subnet_ids == resource.get("subnet_ids")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="remove_tag_subnet", depends=["add_tag_subnet"])
async def test_remove_tag_subnet(hub, ctx, __test):
    global PARAMETER
    # idem state --test update remove subnet and tags
    ctx["test"] = __test
    # new_parameter = copy.deepcopy(PARAMETER)
    resource_id = PARAMETER["resource_id"]
    new_tags = PARAMETER["tags"]
    subnet_ids = []

    ret = await hub.states.aws.ec2.vpc_endpoint.present(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
        vpc_id=PARAMETER["vpc_id"],
        tags=new_tags,
        service_name=f"com.amazonaws.{PARAMETER['region_name']}.s3",
        private_dns_enabled=False,
        vpc_endpoint_type="Interface",
        subnet_ids=subnet_ids,
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            f"Would succeed in modifying aws.ec2.vpc_endpoint '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Succeeded modifying aws.ec2.vpc_endpoint '{PARAMETER['name']}'"
            in ret["comment"]
        )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")
    assert not resource.get("subnet_ids")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["remove_tag_subnet"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test

    ret = await hub.states.aws.ec2.vpc_endpoint.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    if __test:
        assert (
            f"Would delete aws.ec2.vpc_endpoint '{PARAMETER['name']}'" in ret["comment"]
        )
    else:
        assert f"Deleted '{PARAMETER['name']}'" in ret["comment"]

    old_state = ret["old_state"]
    assert PARAMETER["vpc_id"] == old_state["vpc_id"]
    assert PARAMETER["resource_id"] == old_state["resource_id"]
    assert PARAMETER["tags"] == old_state.get("tags")
    assert f"com.amazonaws.{PARAMETER['region_name']}.s3" == old_state["service_name"]
    assert "Interface" == old_state["vpc_endpoint_type"]
    assert not old_state["private_dns_enabled"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    # delete already deleted resource
    ret = await hub.states.aws.ec2.vpc_endpoint.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    # the endpoint can be completely gone since it only stays in the "deleted" stage for a few seconds
    assert (
        f"aws.ec2.vpc_endpoint '{PARAMETER['name']}' already absent" in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_vpc_endpoint_absent_with_none_resource_id(hub, ctx):
    vpc_endpoint_temp_name = "idem-test-vpc-endpoint" + str(uuid.uuid4())
    # Delete vpc with resource_id as None. Result in no-op.
    ret = await hub.states.aws.ec2.vpc_endpoint.absent(
        ctx, name=vpc_endpoint_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.ec2.vpc_endpoint '{vpc_endpoint_temp_name}' already absent"
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ec2.vpc_endpoint.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
