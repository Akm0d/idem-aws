import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_cost_category(hub, ctx):
    # Skipping this as Cost Category is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return

    cost_category_temp_name = "idem-test-" + str(uuid.uuid4())
    rule_version = "CostCategoryExpression.v1"

    rules = [
        {
            "InheritedValue": {"DimensionName": "LINKED_ACCOUNT_NAME"},
            "Type": "INHERITED_VALUE",
        },
        {
            "Rule": {
                "Dimensions": {
                    "Key": "RECORD_TYPE",
                    "MatchOptions": ["EQUALS"],
                    "Values": ["Credit"],
                }
            },
            "Type": "REGULAR",
            "Value": "rule 2",
        },
        {
            "Rule": {
                "Dimensions": {
                    "Key": "SERVICE_CODE",
                    "MatchOptions": ["EQUALS"],
                    "Values": ["AWSBudgets"],
                }
            },
            "Type": "REGULAR",
            "Value": "rule 3",
        },
        {
            "Rule": {
                "CostCategories": {
                    "Key": "test",
                    "MatchOptions": ["EQUALS"],
                    "Values": ["rule"],
                }
            },
            "Type": "REGULAR",
            "Value": "rule 4",
        },
    ]

    split_charge_rules = [
        {
            "Source": "rule 2",
            "Targets": ["rule 3", "rule 4"],
            "Method": "FIXED",
            "Parameters": [{"Type": "ALLOCATION_PERCENTAGES", "Values": ["40", "60"]}],
        }
    ]
    default_value = "Other"
    tags = {"name": "test_category"}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create Cost Category with Test Flag
    ret = await hub.states.aws.costexplorer.cost_category.present(
        test_ctx,
        name=cost_category_temp_name,
        rule_version=rule_version,
        rules=rules,
        cost_category_name=cost_category_temp_name,
        split_charge_rules=split_charge_rules,
        default_value=default_value,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.costexplorer.cost_category '{cost_category_temp_name}'"
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert rule_version == resource.get("rule_version")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        rules, resource.get("rules")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        split_charge_rules, resource.get("split_charge_rules")
    )

    assert cost_category_temp_name == resource.get("cost_category_name")
    assert default_value == resource.get("default_value")
    assert tags == resource.get("tags")

    # Create Cost Category with real
    ret = await hub.states.aws.costexplorer.cost_category.present(
        ctx,
        name=cost_category_temp_name,
        rule_version=rule_version,
        rules=rules,
        cost_category_name=cost_category_temp_name,
        split_charge_rules=split_charge_rules,
        default_value=default_value,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    resource_id = resource.get("resource_id")
    assert rule_version == resource.get("rule_version")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        rules, resource.get("rules")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        split_charge_rules, resource.get("split_charge_rules")
    )
    assert cost_category_temp_name == resource.get("cost_category_name")
    assert default_value == resource.get("default_value")
    assert tags == resource.get("tags")

    # Describe Cost Category
    describe_ret = await hub.states.aws.costexplorer.cost_category.describe(
        ctx,
    )
    assert resource_id in describe_ret
    assert "aws.costexplorer.cost_category.present" in describe_ret.get(resource_id)
    described_resource = describe_ret.get(resource_id).get(
        "aws.costexplorer.cost_category.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "name" in described_resource_map
    assert resource_id == described_resource_map["name"]
    assert rule_version == described_resource_map.get("rule_version")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        rules, described_resource_map.get("rules")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        split_charge_rules, described_resource_map.get("split_charge_rules")
    )
    assert cost_category_temp_name == described_resource_map.get("cost_category_name")
    assert default_value == described_resource_map.get("default_value")
    assert tags == described_resource_map.get("tags")

    # Update Cost Category in test
    rules = [
        {
            "Rule": {
                "Dimensions": {
                    "Key": "RECORD_TYPE",
                    "MatchOptions": ["EQUALS"],
                    "Values": ["Credit"],
                }
            },
            "Type": "REGULAR",
            "Value": "rule 2",
        },
        {
            "Rule": {
                "Dimensions": {
                    "Key": "SERVICE_CODE",
                    "MatchOptions": ["EQUALS"],
                    "Values": ["AWSBudgets"],
                }
            },
            "Type": "REGULAR",
            "Value": "rule 3",
        },
    ]

    split_charge_rules = [{"Source": "rule 2", "Targets": ["rule 3"], "Method": "EVEN"}]
    default_value = "Miscellaneous"
    tags[
        f"idem-test-cost-category-key-{str(uuid.uuid4())}"
    ] = f"idem-test-cost-category-value-{str(uuid.uuid4())}"

    ret = await hub.states.aws.costexplorer.cost_category.present(
        test_ctx,
        name=cost_category_temp_name,
        resource_id=resource_id,
        rule_version=rule_version,
        rules=rules,
        cost_category_name=cost_category_temp_name,
        split_charge_rules=split_charge_rules,
        default_value=default_value,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        rules, resource.get("rules")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        split_charge_rules, resource.get("split_charge_rules")
    )
    assert default_value == resource.get("default_value")
    assert tags == resource.get("tags")

    # Update Cost Category in real
    ret = await hub.states.aws.costexplorer.cost_category.present(
        ctx,
        name=cost_category_temp_name,
        resource_id=resource_id,
        rule_version=rule_version,
        rules=rules,
        cost_category_name=cost_category_temp_name,
        split_charge_rules=split_charge_rules,
        default_value=default_value,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        rules, resource.get("rules")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        split_charge_rules, resource.get("split_charge_rules")
    )
    assert cost_category_temp_name == resource.get("cost_category_name")
    assert default_value == resource.get("default_value")
    real_tags = resource.get("tags")
    assert real_tags
    assert 2 == len(real_tags)
    assert tags == resource.get("tags")

    # Delete Cost Category with test flag
    ret = await hub.states.aws.costexplorer.cost_category.absent(
        test_ctx, name=cost_category_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.costexplorer.cost_category '{cost_category_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert rule_version == resource.get("rule_version")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        rules, resource.get("rules")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        split_charge_rules, resource.get("split_charge_rules")
    )
    assert default_value == resource.get("default_value")

    # Delete Cost Category in real
    ret = await hub.states.aws.costexplorer.cost_category.absent(
        ctx, name=cost_category_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Deleted aws.costexplorer.cost_category '{cost_category_temp_name}'"
        in ret["comment"]
    )
    resource = ret.get("old_state")
    assert rule_version == resource.get("rule_version")
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        rules, resource.get("rules")
    )
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        split_charge_rules, resource.get("split_charge_rules")
    )

    assert default_value == resource.get("default_value")

    # Delete Cost Category again
    ret = await hub.states.aws.costexplorer.cost_category.absent(
        ctx, name=cost_category_temp_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.costexplorer.cost_category '{cost_category_temp_name}' already absent"
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_cost_category_absent_with_none_resource_id(hub, ctx):
    cost_category_temp_name = "idem-test-cost-category-" + str(uuid.uuid4())
    # Delete Cost Category with resource_id as None. Result in no-op.
    ret = await hub.states.aws.costexplorer.cost_category.absent(
        ctx, name=cost_category_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.costexplorer.cost_category '{cost_category_temp_name}' already absent"
        in ret["comment"]
    )
