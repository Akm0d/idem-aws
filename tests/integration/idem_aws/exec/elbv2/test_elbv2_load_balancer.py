import time

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_resource_id(hub, ctx):
    lb_get_name = "idem-test-exec-get-elbv2-lb-" + str(int(time.time()))
    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name=lb_get_name,
        resource_id="invalid-arn",
    )
    assert not ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"ClientError: An error occurred (ValidationError) when calling the DescribeLoadBalancers operation: "
        f"'invalid-arn' is not a valid load balancer ARN" in str(ret["comment"])
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_get_invalid_load_balancer_name(hub, ctx):
    ret = await hub.exec.aws.elbv2.load_balancer.get(
        ctx,
        name="invalid-name-xyz-abc",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        f"LoadBalancerNotFoundException: An error occurred (LoadBalancerNotFound) when calling the DescribeLoadBalancers"
        f" operation: Load balancers '[invalid-name-xyz-abc]' not found"
        in str(ret["comment"])
    )
